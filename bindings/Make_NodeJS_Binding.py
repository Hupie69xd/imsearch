#!/bin/env python3
# Make_NodeJS_Binding.py
""" ImSearch
   Copyright (C) 2022  Hupie (hupiew[at]gmail.com)

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. """

import re
from dataclasses import dataclass


def _format_checks(statements, max_len):
    statements = _flatten_list(statements)
    statements = list(filter(lambda x: x is not None, statements))

    length = sum([len(i) for i in statements])
    length += (len(statements) - 1) * 4 # || joining the expressions
    length += len("    if ()")

    if max_len < length:
        return " || \n        ".join(statements)
    else:
        return " || ".join(statements)

def _joinable_on_lastline(line, last, max_len):
    if not (line.startswith(".") or last.endswith("=")):
        return False
    return len(line) + len(last) <= max_len

def indent(s, max_len, spaces=4):
    """ Don't use this, it only work for code written with this function in mind. """
    formated = []
    level = 0
    last = ""
    for i in s.split('\n'):
        line = i.strip()
        if line == "":
            formated.append(line)
            continue
        elif _joinable_on_lastline(line, last, max_len):
            formated[-1] += line
            last = formated[-1]
            continue
        off = line.startswith(".") - line.startswith("}") + last.endswith("=")

        no_indent = not line.startswith("#")
        a_spaces = no_indent * spaces * (level + off)
        formated.append(' ' * a_spaces + line)
        # kind of a hacky way of doing this tbqh
        level += line.count("{") + line.count("(") - line.count(")") - line.count("}")
        last = formated[-1]
    return "\n".join(formated)

def _make_return(types, func, returntype, throws=False):
    ret_statement = "\n".join(returntype.return_statement())
    assign = returntype.return_assignment()
    return_code =  "\n".join(returntype.return_code())

    args = ", ".join([t.get_function_arg(n) for n, t in enumerate(types)])
    return f"""{assign}{func}({args});
{return_code}
return {ret_statement};"""

def _object_checks(types, max_len):
    objects = [t.get_null_check(n) for n, t in enumerate(types)]

    null_checks = _format_checks(objects, max_len)

    if len(null_checks) == 0:
        return ""

    return f"""
#if defined(NAPI_DISABLE_CPP_EXCEPTIONS)
if ({null_checks})
{{
return env.Null();
}}
#endif
"""

def _try_catch():
    return "try\n{\n", '\n'.join(["}",
                                  "catch (const std::exception& e)",
                                  "{",
                                  "Napi::Error::New(env, e.what())",
                                  ".ThrowAsJavaScriptException();",
                                  "return env.Null();",
                                  "}"])

def _single_arg_check(type_, type_checks):
    arg_type = type_.node_type()
    return f"""
if (info.Length() < 1 || {type_checks})
{{
  Napi::TypeError::New(env, "Expected a {arg_type}.")
    .ThrowAsJavaScriptException();
  return env.Null();
}}"""


def _flatten_list(l):
    out = []
    for i in l:
        if isinstance(i, list):
            out.extend(i)
        else:
            out.append(i)
    return out

# function_body([TypedArray(), String()], "TraceConvert::trace_to_iscf", Vector("uint8_t"))
def function_body(types, func, returntype, max_line_length=88, throws=False):
    """ Generates the boilerplate for the Node bindings. With a few changes depending
        on the code that's being written, this should get you 90% of the way there.
        `Types` types is a list of classes that inherit from _Type,
          these are the function arguments.
        `func` is a string that is the name of the c++ function that being called.
        `returntype` is the type that's returned from the c++ function `func`.
        `max_line_length` Is used for formatting is some (meaning not all) places.
        `throws` Indicates if the c++ fuctions throws, if True generates code to wrap
          these errors and throw them as javascript errors.
          """
    length = len(types)
    type_checks = _format_checks([t.get_type_check(n) for n, t in enumerate(types)],
                                 max_line_length)
    type_casts = "\n".join(_flatten_list([t.get_cast(n) for n, t in enumerate(types)]))
    return_statement = _make_return(types, func, returntype)
    object_checks = _object_checks(types, max_line_length)
    if throws:
        try_block, catch_block = _try_catch()
    else:
        try_block, catch_block = "", ""
    
    if length == 0:
        argument_checks = ""
    elif length == 1:
        argument_checks = _single_arg_check(types[0], type_checks)
    else:
        argument_checks = f"""
if (info.Length() < {length})
{{
  Napi::TypeError::New(env, "Wrong number of arguments")
    .ThrowAsJavaScriptException();
  return env.Null();
}}
  if ({type_checks})
{{
  Napi::TypeError::New(env, "Wrong arguments")
    .ThrowAsJavaScriptException();
  return env.Null();
}}"""

    func = '\n'.join(["Napi::Value function(const Napi::CallbackInfo& info)",
                      "{",
                      "Napi::Env env = info.Env();",
                      "",
                      argument_checks,
                      "",
                      type_casts,
                      "",
                      object_checks,
                      "",
                      try_block,
                      return_statement,
                      catch_block,
                      "}"]).replace("\n\n", '\n')
    return indent(func, max_line_length)




#################### Types ####################


class _Type:
    TO_FUNC = ""
    TYPE_CHECK = "!info[{n}].Is{node_type}()"
    CAST = "    auto arg{n} = info[{n}].As<Napi::{node_type}>(){to_value};"
    def node_type(self):
        return self.NODE_TYPE

    def return_assignment(self):
        return "auto ret = "

    def return_code(self):
        return ""

    def return_statement(self):
        node_type = self.node_type()
        return [f"Napi::{node_type}::New(env, ret)", ]

    def value_function(self):
        return self.TO_FUNC

    def get_function_arg(self, n):
        return f"arg{n}"

    def get_type_check(self, n):
        return self.TYPE_CHECK.format(n=n, node_type=self.node_type())

    def get_null_check(self, n):
        return None

    def get_cast(self, n, **kwargs):
        rt = self.CAST.format(n = n,
                              node_type = self.node_type(),
                              to_value = self.value_function(),
                              **kwargs)
        return [rt, ]

class String(_Type):
    NODE_TYPE = "String"
    TO_FUNC = ".Utf8Value()"

class Null(_Type):
    def return_assignment(self):
        return ""
    def return_statement(self):
        return ["env.Undefined()", ]

@dataclass
class Object(_Type):
    cls: str

    CAST = "auto* arg{n} = \
\n  Napi::ObjectWrap<{cls}>::Unwrap(info[{n}].As<Napi::Object>());"
    NODE_TYPE = "Object"
    NULL_CHECK = "arg{n} == nullptr"
    def get_cast(self, n):
        return super().get_cast(n, cls=self.cls)
    def get_null_check(self, n):
        return self.NULL_CHECK.format(n=n)

@dataclass
class TypedArray(_Type):
    ptr_type: str = "void"
    type_size: int = 1
    NODE_TYPE = "TypedArray"
    TO_FUNC = ".ArrayBuffer()"

    def get_function_arg(self, n):
        return f"ptr{n}, size{n}"

    def get_cast(self, n):
        if 1 < self.type_size:
            divide = f" / {self.type_size}"
        else:
            divide = ""
        if self.ptr_type == "void":
            cast = f" arg{n}.Data();"
        else:
            cast = f"reinterpret_cast<{self.ptr_type}*>(arg{n}.Data());"
        return super().get_cast(n) + \
               [f"{self.ptr_type}* ptr{n} ={cast}",
                f"size_t size{n} = arg{n}.ByteLength(){divide};"]

class Vector(TypedArray):

    def _infer_type(self): # eww nasty
        sizes = ['8', '16', '32', '64']
        t = self.ptr_type.lower()
        is_unsigned = t.startswith("u")
        is_int = not t.startswith("float")
        m = re.search("[0-9]+", t)
        assert m is not None, "No size specified."
        val = m.group()
        assert val in sizes, "Invalid numeral size."

        if (not is_int) and val in sizes[2:]:
            return f"Float{val}Array"
        elif is_int and val in sizes[:-1]:
            s = f"int{val}Array"
            if is_unsigned:
                s = "U" + s
            return s.title()

        raise RuntimeError("Unable to determine type.")

    def return_statement(self):
        return ["ret_arr", ]

    def return_code(self):
        array_type = self._infer_type()
        return ["size_t ret_size = ret.size();",
                f"auto ret_arr = Napi::{array_type}::New(env, ret_size);",
                "std::copy(ret.cbegin(), ret.cend(), ret_arr.Data());"]


class Boolean(_Type):
    NODE_TYPE = "Boolean"
    TO_FUNC = ".Value()"

class _Number(_Type):
    NODE_TYPE = "Number"
class UInt32(_Number):
    TO_FUNC = ".Uint32Value()"
class Int32(_Number):
    TO_FUNC = ".Int32Value()"
class Int64(_Number):
    TO_FUNC = ".Int64Value()"
class Double(_Number):
    TO_FUNC = ".DoubleValue()"
class Float(_Number):
    TO_FUNC = ".FloatValue()"
