// NodeIscFile.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/nodejs/NodeIscFile.hpp>

#include <napi.h>


namespace imsearch {


Napi::Object NodeIscFile::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func =
        DefineClass(env,
            "IscFile",
            { InstanceMethod("addIndex", &NodeIscFile::AddIndex),
              InstanceMethod("setName", &NodeIscFile::SetName),
              InstanceMethod("setZipId", &NodeIscFile::SetZipId),
              InstanceMethod("setFps", &NodeIscFile::SetFps),
              InstanceMethod("toFile", &NodeIscFile::ToFile)
            });

    Napi::FunctionReference* constructor = new Napi::FunctionReference();
    *constructor = Napi::Persistent(func);
    env.SetInstanceData(constructor);

    exports.Set("IscFile", func);
    return exports;
}


Napi::Value NodeIscFile::AddIndex(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsObject())
    {
        Napi::TypeError::New(env, "Expected a IscIndex.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto* arg0 = Napi::ObjectWrap<NodeIscIndex>::Unwrap(info[0].As<Napi::Object>());

    isc.add_index(arg0->get_index());
    return env.Undefined();
}

Napi::Value NodeIscFile::SetName(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsString())
    {
        Napi::TypeError::New(env, "Expected a String.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::String>().Utf8Value();

    isc.set_name(arg0);
    return env.Undefined();
}

Napi::Value NodeIscFile::SetZipId(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsString())
    {
        Napi::TypeError::New(env, "Expected a String.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::String>().Utf8Value();

    isc.set_zip_id(arg0);
    return env.Undefined();
}

Napi::Value NodeIscFile::SetFps(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsNumber())
    {
        Napi::TypeError::New(env, "Expected a Number.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().FloatValue();

    isc.set_fps(arg0);
    return env.Undefined();
}

Napi::Value NodeIscFile::ToFile(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    auto ret = isc.to_file();
    size_t ret_size = ret.size();
    auto ret_arr = Napi::Uint8Array::New(env, ret_size);
    std::copy(ret.cbegin(), ret.cend(), ret_arr.Data());
    return ret_arr;
}


Napi::Object NodeIscIndex::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func =
        DefineClass(env,
            "IscIndex",
            { InstanceMethod("addHashData", &NodeIscIndex::AddHashData),
              InstanceMethod("addImageIndex", &NodeIscIndex::AddImageIndex),
              InstanceMethod("addTimeIndex", &NodeIscIndex::AddTimeIndex),
              InstanceMethod("addTitle", &NodeIscIndex::AddTitle),
              InstanceMethod("setDescriptorLength", &NodeIscIndex::SetDescriptorLength),
              InstanceMethod("setType", &NodeIscIndex::SetType)
            });

    Napi::FunctionReference* constructor = new Napi::FunctionReference();
    *constructor = Napi::Persistent(func);
    env.SetInstanceData(constructor);

    exports.Set("IscIndex", func);
    return exports;
}

Napi::Value NodeIscIndex::AddHashData(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsTypedArray())
    {
        Napi::TypeError::New(env, "Expected a TypedArray.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint8_t* ptr0 = reinterpret_cast<uint8_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength();

    isc.add_hashdata({ ptr0, size0 });
    return env.Undefined();
}

Napi::Value NodeIscIndex::AddImageIndex(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsTypedArray())
    {
        Napi::TypeError::New(env, "Expected a TypedArray.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint32_t* ptr0 = reinterpret_cast<uint32_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength() / 4;

    isc.add_imageindex(ptr0, size0);
    return env.Undefined();
}

Napi::Value NodeIscIndex::AddTimeIndex(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsTypedArray())
    {
        Napi::TypeError::New(env, "Expected a TypedArray.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint32_t* ptr0 = reinterpret_cast<uint32_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength() / 4;

    isc.add_timeindex(ptr0, size0);
    return env.Undefined();
}

Napi::Value NodeIscIndex::AddTitle(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 2)
    {
        Napi::TypeError::New(env, "Wrong number of arguments")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    if (!info[0].IsString() || !info[1].IsNumber())
    {
        Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::String>().Utf8Value();
    auto arg1 = info[1].As<Napi::Number>().Int32Value();

    isc.add_title(arg0, arg1);
    return env.Undefined();
}

Napi::Value NodeIscIndex::SetDescriptorLength(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsNumber())
    {
        Napi::TypeError::New(env, "Expected a Number.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int32Value();

    isc.set_descriptor_length(arg0);
    return env.Undefined();
}

Napi::Value NodeIscIndex::SetType(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsNumber())
    {
        Napi::TypeError::New(env, "Expected a Number.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int32Value();

    if (arg0 < 0 || 255 < arg0)
    {
        Napi::TypeError::New(env, "Type has to be an integer [0, 256)")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    isc.set_type(arg0);
    return env.Undefined();
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
