// NodeMain.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/nodejs/NodeMain.hpp>

#include <bindings/nodejs/NodeIscFile.hpp>
#include <bindings/nodejs/NodeUtil.hpp>
#include <bindings/nodejs/NodeStdPlugins.hpp>
#include <bindings/nodejs/NodeSimpleFinder.hpp>
#include <bindings/nodejs/NodeTracemoeFinder.hpp>
#include <ImSearchConfig.hpp>

#include <napi.h>


namespace imsearch {


Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
    NodeMain::Init(env, exports);
    NodeIscIndex::Init(env, exports);
    NodeIscFile::Init(env, exports);
    NodeUtil::Init(env, exports);
    NodeFinderPlugin::Init(env, exports);
    NodeStdPlugins::Init(env, exports);
    NodeSimpleFinder::Init(env, exports);
    NodeTracemoeFinder::Init(env, exports);
    return exports;
}


NODE_API_MODULE(imsearch, InitAll)


Napi::Object NodeMain::Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(Napi::String::New(env, "version"), Napi::Function::New(env, version));
    return exports;
}

Napi::String NodeMain::version(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    return Napi::String::New(env, IMSEARCH_VERSION_STRING);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
