// NodeSimpleFinder.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/nodejs/NodeSimpleFinder.hpp>

#include <napi.h>

#include <descriptors/SimpleHash.hpp>


namespace imsearch {


Napi::Object NodeSimpleFinder::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func =
        DefineClass(env,
            simplefinder_name,
            { InstanceMethod("loadFile", &NodeSimpleFinder::load_file),
              InstanceMethod("addPlugin", &NodeSimpleFinder::add_plugin),
              InstanceMethod("buildAnn", &NodeSimpleFinder::build_ann),
              InstanceMethod("loadAnn", &NodeSimpleFinder::load_ann),
              InstanceMethod("searchAnn", &NodeSimpleFinder::search_ann),
              InstanceMethod("clear", &NodeSimpleFinder::clear),
              InstanceAccessor("length", &NodeSimpleFinder::size, nullptr),
              InstanceMethod("at", &NodeSimpleFinder::at),
              InstanceMethod("getResults", &NodeSimpleFinder::get_results)
            });

    Napi::FunctionReference* constructor = new Napi::FunctionReference();
    *constructor = Napi::Persistent(func);
    env.SetInstanceData(constructor);

    exports.Set(simplefinder_name, func);
    return exports;
}


Napi::Value NodeSimpleFinder::build_ann(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 2)
    {
        Napi::TypeError::New(env, "Wrong number of arguments")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    if (!info[0].IsNumber() || !info[1].IsNumber())
    {
        Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int32Value();
    auto arg1 = info[1].As<Napi::Number>().Int32Value();

    try
    {
        finder.build_ann(arg0, arg1);
        return env.Undefined();
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Value NodeSimpleFinder::load_ann(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsString())
    {
        Napi::TypeError::New(env, "Expected a String.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::String>().Utf8Value();

    try
    {
        auto ret = finder.load_ann(arg0);
        return Napi::Boolean::New(env, ret);
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Value NodeSimpleFinder::search_ann(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 2)
    {
        Napi::TypeError::New(env, "Wrong number of arguments")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    if (!info[0].IsNumber() || !info[1].IsNumber())
    {
        Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int64Value();
    auto arg1 = info[1].As<Napi::Number>().Int32Value();

    try
    {
        auto ret = finder.search_ann(arg0, arg1);
        return results_to_js(env, ret);
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Value NodeSimpleFinder::wrapped_search(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsNumber())
    {
        Napi::TypeError::New(env, "Expected a Number.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int64Value();
    auto arg1 = info[1].As<Napi::Number>().FloatValue();
    auto arg2 = info[2].As<Napi::Number>().Int32Value();
    auto arg3 = info[3].As<Napi::Boolean>().Value();

    auto ret = finder.get_results(
        { *reinterpret_cast<uint64_t*>(&arg0) }, arg1, arg2, arg3);
    return results_to_js(env, ret);
}


Napi::Value NodeSimpleFinder::wrapped_at(const Napi::CallbackInfo& info,
                                         int64_t index)
{
    Napi::Env env = info.Env();

    auto ret = finder.at(index).hash;
    /* I have no idea how I want to deal with js numbers. Do I convert it 
    to a double, or reinterpret as double so the bits stay the same... */
    return Napi::Number::New(env, *reinterpret_cast<double*>(&ret));
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
