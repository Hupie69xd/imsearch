// NodeStdPlugins.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <memory>
#include <sstream>

#include <napi.h>

#include <plugins/FinderPlugin.hpp>
#include <plugins/ImagePlugin.hpp>
#include <plugins/NamePlugin.hpp>
#include <plugins/TimePlugin.hpp>


namespace imsearch {


class NodeFinderPlugin
    : public Napi::ObjectWrap<NodeFinderPlugin>
{
    std::shared_ptr<FinderPlugin> plugin;

public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);

    NodeFinderPlugin(const Napi::CallbackInfo& info)
        : Napi::ObjectWrap<NodeFinderPlugin>(info), plugin() {}

    std::shared_ptr<FinderPlugin> get_plugin() const { return plugin; }

    void set_plugin(std::shared_ptr<FinderPlugin> p) { plugin = p; }

private:
    Napi::Value clear(const Napi::CallbackInfo& info);
    Napi::Value retrive_result(const Napi::CallbackInfo& info);
};


class NodeStdPlugins
{
public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports);

    static Napi::Value init_ImagePlugin(const Napi::CallbackInfo& info);
    static Napi::Value init_NamePlugin(const Napi::CallbackInfo& info);
    static Napi::Value init_TimePlugin(const Napi::CallbackInfo& info);

    static Napi::Value init_plugin(const Napi::CallbackInfo& info,
                                   std::shared_ptr<FinderPlugin> p);
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
