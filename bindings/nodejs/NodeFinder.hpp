// NodeFinder.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <bindings/nodejs/NodeStdPlugins.hpp>
#include <FinderTemplate.hpp>
#include <plugins/FinderPlugin.hpp>

#include <napi.h>


namespace imsearch {


template<typename Finder>
class NodeFinder
{
public:
    Finder finder;

    NodeFinder() : finder(Finder()) {}

    static Napi::Value results_to_js(
        Napi::Env env, const std::vector<FinderPlugin::dict_t>& results)
    {
        auto arr = Napi::Array::New(env, results.size());
        for (auto i = 0; i < results.size(); ++i)
        {
            const auto& item = results[i];
            auto ob = Napi::Object::New(env);
            for (const auto& pair : item)
            {
                ob[pair.first] = Napi::String::New(env, pair.second);
            }
            arr[i] = ob;
        }
        return arr;
    }

protected:
    virtual Napi::Value wrapped_at(const Napi::CallbackInfo& info, int64_t) = 0;
    virtual Napi::Value wrapped_search(const Napi::CallbackInfo& info) = 0;

    Napi::Value get_results(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();

        if (info.Length() < 4)
        {
            Napi::TypeError::New(env, "Wrong number of arguments")
                .ThrowAsJavaScriptException();
            return env.Null();
        }
        if (!info[1].IsNumber() ||
            !info[2].IsNumber() ||
            !info[3].IsBoolean())
        {
            Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
            return env.Null();
        }

        return this->wrapped_search(info);
    }

    Napi::Value at(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();

        if (info.Length() < 1 || !info[0].IsNumber())
        {
            Napi::TypeError::New(env, "Expected a Number.")
                .ThrowAsJavaScriptException();
            return env.Null();
        }
        auto arg0 = info[0].As<Napi::Number>().Int64Value();

        try
        {
            return this->wrapped_at(info, arg0);
        }
        catch (const std::exception& e)
        {
            Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
            return env.Null();
        }
    }

    Napi::Value load_file(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();

        if (info.Length() < 2)
        {
            Napi::TypeError::New(env, "Wrong number of arguments")
                .ThrowAsJavaScriptException();
            return env.Null();
        }
        if (!info[0].IsTypedArray() || !info[1].IsNumber())
        {
            Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
            return env.Null();
        }

        auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
        char* ptr0 = reinterpret_cast<char*>(arg0.Data());
        size_t size0 = arg0.ByteLength();
        auto arg1 = info[1].As<Napi::Number>().Int32Value();
        
        try
        {
            auto ret = finder.load_cfile({ ptr0, size0 }, arg1);
            return Napi::Number::New(env, ret);
        }
        catch (const std::exception& e)
        {
            Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
            return env.Null();
        }
    }

    Napi::Value add_plugin(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();

        if (info.Length() < 1 || !info[0].IsObject())
        {
            Napi::TypeError::New(env, "Expected a FinderPlugin.")
                .ThrowAsJavaScriptException();
            return env.Null();
        }

        try
        {
            auto* obj0 = Napi::ObjectWrap<NodeFinderPlugin>::Unwrap(
                    info[0].As<Napi::Object>());
            auto arg0 = obj0->get_plugin();
            finder.add_plugin(arg0);
            return env.Undefined();
        }
        catch (const Napi::Error& e)
        {
            Napi::TypeError::New(env, "Unable to unwrap arg0 as FinderPlugin.")
                .ThrowAsJavaScriptException();
            return env.Null();
        }
        catch (const std::exception& e)
        {
            Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
            return env.Null();
        }
    }

    Napi::Value clear(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();

        if (info.Length() < 1 || !info[0].IsBoolean())
        {
            Napi::TypeError::New(env, "Expected a Boolean.")
                .ThrowAsJavaScriptException();
            return env.Null();
        }
        auto arg0 = info[0].As<Napi::Boolean>().Value();

        finder.clear(arg0);
        return env.Undefined();
    }


    Napi::Value size(const Napi::CallbackInfo& info)
    {
        Napi::Env env = info.Env();
        auto ret = finder.size();
        return Napi::Number::New(env, ret);
    }
};

} // namespace imsearch
/**
 * Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
 *
 *  This file is part of the ImSearch.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Or alternatively, under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation, either version
 *  3 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU (Lesser) General Public License for more details.
 *
 *  You should have received a copy of the GNU (Lesser) General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
