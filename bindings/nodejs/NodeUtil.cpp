// NodeUtil.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/nodejs/NodeUtil.hpp>

#include <algorithm>
#include <string>

#include <util/Lz4File.hpp>
#include <util/TraceConvert.hpp>

#include <napi.h>

namespace imsearch {


Napi::Value NodeUtil::TraceToIscf(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 2)
    {
        Napi::TypeError::New(env, "Wrong number of arguments")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    if (!info[0].IsTypedArray() || !info[1].IsString())
    {
        Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint8_t* ptr0 = reinterpret_cast<uint8_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength();
    auto arg1 = info[1].As<Napi::String>().Utf8Value();

    try
    {
        auto ret = TraceConvert::trace_to_iscf({ ptr0, size0 }, arg1);
        size_t ret_size = ret.size();
        auto ret_arr = Napi::Uint8Array::New(env, ret_size);
        std::copy(ret.cbegin(), ret.cend(), ret_arr.Data());
        return ret_arr;
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Value NodeUtil::CompressLz4(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 2)
    {
        Napi::TypeError::New(env, "Wrong number of arguments")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    if (!info[0].IsTypedArray() || !info[1].IsNumber())
    {
        Napi::TypeError::New(env, "Wrong arguments").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint8_t* ptr0 = reinterpret_cast<uint8_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength();
    auto arg1 = info[1].As<Napi::Number>().Int32Value();

    try
    {
        auto ret = Lz4File::compress({ ptr0, size0 }, arg1);
        size_t ret_size = ret.size();
        auto ret_arr = Napi::Uint8Array::New(env, ret_size);
        std::copy(ret.cbegin(), ret.cend(), ret_arr.Data());
        return ret_arr;
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Value NodeUtil::DecompressLz4(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsTypedArray())
    {
        Napi::TypeError::New(env, "Expected a TypedArray.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::TypedArray>().ArrayBuffer();
    uint8_t* ptr0 = reinterpret_cast<uint8_t*>(arg0.Data());
    size_t size0 = arg0.ByteLength();

    try
    {
        auto ret = Lz4File::decompress({ ptr0, size0 });
        size_t ret_size = ret.size();
        auto ret_arr = Napi::Uint8Array::New(env, ret_size);
        std::copy(ret.cbegin(), ret.cend(), ret_arr.Data());
        return ret_arr;
    }
    catch (const std::exception& e)
    {
        Napi::Error::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }
}

Napi::Object NodeUtil::Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(Napi::String::New(env, "compressLz4"), Napi::Function::New(env, CompressLz4));
    exports.Set(Napi::String::New(env, "decompressLz4"), Napi::Function::New(env, DecompressLz4));
    exports.Set(Napi::String::New(env, "traceToIscf"), Napi::Function::New(env, TraceToIscf));
    return exports;
}


} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
