// NodeStdPlugins.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/nodejs/NodeStdPlugins.hpp>

#include <plugins/FinderPlugin.hpp>
#include <plugins/ImagePlugin.hpp>
#include <plugins/NamePlugin.hpp>
#include <plugins/TimePlugin.hpp>

#include <napi.h>


namespace imsearch {

Napi::Object NodeFinderPlugin::Init(Napi::Env env, Napi::Object exports)
{
    Napi::Function func =
        DefineClass(env,
            FinderPlugin::plugin_name,
            { InstanceMethod("clear", &NodeFinderPlugin::clear),
              InstanceMethod("retrieveResult", &NodeFinderPlugin::retrive_result) });

    Napi::FunctionReference* constructor = new Napi::FunctionReference();
    *constructor = Napi::Persistent(func);
    env.SetInstanceData(constructor);

    exports.Set(FinderPlugin::plugin_name, func);
    return exports;
}

Napi::Value NodeFinderPlugin::clear(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();
    if (!plugin)
    {
        Napi::TypeError::New(env, "Plugin is null.").ThrowAsJavaScriptException();
        return env.Null();
    }
    plugin->clear();
    return env.Undefined();
}

Napi::Value NodeFinderPlugin::retrive_result(const Napi::CallbackInfo& info)
{
    Napi::Env env = info.Env();

    if (!plugin)
    {
        Napi::TypeError::New(env, "Plugin is null.").ThrowAsJavaScriptException();
        return env.Null();
    }
    if (info.Length() < 1 || !info[0].IsNumber())
    {
        Napi::TypeError::New(env, "Expected a Number.").ThrowAsJavaScriptException();
        return env.Null();
    }
    auto arg0 = info[0].As<Napi::Number>().Int64Value();

    auto ret = TimePlugin::dict_t{};
    try
    {
        ret = plugin->retrive_result(arg0, ret);
    }
    catch (const std::exception& e)
    {
        Napi::TypeError::New(env, e.what()).ThrowAsJavaScriptException();
        return env.Null();
    }

    std::string values{ "" };
    if (0 < ret.size())
    {
        std::stringstream out{};
        for (const auto& i : ret)
        {
            out << '"' << i.first << "\": \"" << i.second << "\", ";
        }
        const auto end = out.str().size() - 2;
        values = out.str().substr(0, end);
    }
    return Napi::String::New(env, values);
}


Napi::Object NodeStdPlugins::Init(Napi::Env env, Napi::Object exports)
{
    {
        exports.Set(Napi::String::New(env, "initImagePlugin"), Napi::Function::New(env, init_ImagePlugin));
        exports.Set(Napi::String::New(env, "initNamePlugin"), Napi::Function::New(env, init_NamePlugin));
        exports.Set(Napi::String::New(env, "initTimePlugin"), Napi::Function::New(env, init_TimePlugin));
        return exports;
    }
}


Napi::Value NodeStdPlugins::init_ImagePlugin(const Napi::CallbackInfo& info)
{
    return init_plugin(info, std::make_shared<ImagePlugin>());
}


Napi::Value NodeStdPlugins::init_NamePlugin(const Napi::CallbackInfo& info)
{
    return init_plugin(info, std::make_shared<NamePlugin>());
}


Napi::Value NodeStdPlugins::init_TimePlugin(const Napi::CallbackInfo& info)
{
    return init_plugin(info, std::make_shared<TimePlugin>());
}


Napi::Value NodeStdPlugins::init_plugin(const Napi::CallbackInfo& info,
                                        std::shared_ptr<FinderPlugin> p)
{
    Napi::Env env = info.Env();

    if (info.Length() < 1 || !info[0].IsObject())
    {
        Napi::TypeError::New(env, "Expected a FinderPlugin.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }

    try
    {
        auto* ptr =
            Napi::ObjectWrap<NodeFinderPlugin>::Unwrap(info[0].As<Napi::Object>());
        ptr->set_plugin(p);
        return Napi::Boolean::New(env, true);
    }
    catch (const Napi::Error&)
    {
        Napi::TypeError::New(env, "Unable to unwrap as FinderPlugin type.")
            .ThrowAsJavaScriptException();
        return env.Null();
    }
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
