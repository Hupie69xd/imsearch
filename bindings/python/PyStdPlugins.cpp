// PyStdPlugins.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/python/PyStdPlugins.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <plugins/FinderPlugin.hpp>
#include <plugins/ImagePlugin.hpp>
#include <plugins/NamePlugin.hpp>
#include <plugins/TimePlugin.hpp>

#include <memory>


namespace imsearch {


constexpr char finder_plugin_name[] = "FinderPlugin";
constexpr char image_plugin_name[] = "ImagePlugin";
constexpr char name_plugin_name[] = "NamePlugin";
constexpr char time_plugin_name[] = "TimePlugin";


void PyStdPlugins::py_init_finerpluginbase(pybind11::module& m)
{
        pybind11::class_<FinderPlugin, std::shared_ptr<FinderPlugin>>(m, finder_plugin_name);
}


void PyStdPlugins::py_init_imageplugin(pybind11::module& m)
{
    m.def("make_img_index", &ImagePlugin::make_index);
    return py_init_plugin<ImagePlugin, image_plugin_name>(m);
}


void PyStdPlugins::py_init_nameplugin(pybind11::module& m)
{
    return py_init_plugin<NamePlugin, name_plugin_name>(m);
}


void PyStdPlugins::py_init_timeplugin(pybind11::module& m)
{
    return py_init_plugin<TimePlugin, time_plugin_name>(m);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
