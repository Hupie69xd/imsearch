// PyTraceConvert.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <pybind11/pybind11.h>

#include <util/BytesView.hpp>
#include <util/TraceConvert.hpp>

#include <string>


namespace imsearch {


struct PyTraceConvert
{
    /* Binding code for the TraceConvert class' function(s) */
    static void py_init_traceconvert(pybind11::module& m);

    static pybind11::bytes trace_to_iscf(std::string indata, std::string infile)
    {
        /* Wrapper for std::string to pybind bytes to avoid it being interpreted as a str */
        auto ret = TraceConvert::trace_to_iscf(imsearch::BytesView(indata), infile);
        return pybind11::bytes(reinterpret_cast<char*>(ret.data()), ret.size());
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
