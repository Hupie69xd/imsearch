// PyStdPlugings.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <pybind11/pybind11.h>

#include <plugins/FinderPlugin.hpp>

#include <memory>


namespace imsearch {


struct PyStdPlugins
{
    static void py_init_finerpluginbase(pybind11::module&);

    static void py_init_imageplugin(pybind11::module&);

    static void py_init_nameplugin(pybind11::module&);

    static void py_init_timeplugin(pybind11::module&);

    template<class T_Plugin, const char name[]>
    static inline void py_init_plugin(pybind11::module& m)
    {
        /* pybind11 export code */
        pybind11::class_<T_Plugin, FinderPlugin, std::shared_ptr<T_Plugin>>(m, name)
            .def(pybind11::init<>())
            .def("clear", &T_Plugin::clear)
            .def("retrive_result", &T_Plugin::retrive_result, pybind11::arg("position"), pybind11::arg("result_dict&"))
            ;
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
