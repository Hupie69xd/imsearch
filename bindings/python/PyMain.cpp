// PyMain.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/python/PyMain.hpp>

#include <ImSearchConfig.hpp>

//#ifdef IMSEARCH_PYTHON_BINDINGS
#include <cmath>  // Python needs this

#include <pybind11/pybind11.h>

#include <bindings/python/PyCommon.hpp>
#include <bindings/python/PyIscFile.hpp>
#include <bindings/python/PySimpleFinder.hpp>
#include <bindings/python/PyTracemoeFinder.hpp>
#include <bindings/python/PyStdPlugins.hpp>
#include <bindings/python/PyTraceConvert.h>


namespace imsearch {


PYBIND11_MODULE(pyimsearch, m)
{
    m.def("version", []() { return IMSEARCH_VERSION_STRING; });

    PySimpleFinder::py_init_simplehash_finder(m);

    PyStdPlugins::py_init_finerpluginbase(m);

    PyStdPlugins::py_init_imageplugin(m);

    PyStdPlugins::py_init_timeplugin(m);

    PyStdPlugins::py_init_nameplugin(m);

    PyTracemoeFinder::py_init_trace_finder(m);

    auto m_util = m.def_submodule("util");

    PyTraceConvert::py_init_traceconvert(m_util);

    PyCommon::py_init_py_common(m_util);

    PyIscFile::py_init_iscfile(m_util);

    PyIscFile::py_init_iscindex(m_util);
}

//#endif
} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
