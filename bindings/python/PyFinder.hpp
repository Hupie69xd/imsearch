// PyFinder.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <util/BytesView.hpp>


namespace imsearch {


struct PyFinder
{
    template <typename FinderClass, const char name[]>
    static auto py_init_findertemplate(pybind11::module& m)
    {
        /* pybind11 export code */
        return pybind11::class_<FinderClass>(m, name)
            .def(pybind11::init<>())
            .def("__len__", &FinderClass::size)
            .def("add_plugin", &FinderClass::add_plugin)
            .def("clear", &FinderClass::clear, pybind11::arg("only_hashes"))
            .def("load_cfile",
                [](FinderClass& f, std::string& data, int index)
                {
                    return f.load_cfile(imsearch::BytesView(data), index);
                },
                 pybind11::arg("bytes"),
                 pybind11::arg("index_type"))
            .def("search", &FinderClass::search);
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
