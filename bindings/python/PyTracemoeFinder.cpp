// PyTracemoeFinder.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/python/PyTracemoeFinder.hpp>

#include <string>

#include <descriptors/TracemoeDesc.hpp>


namespace imsearch {


void PyTracemoeFinder::py_init_trace_finder(pybind11::module& m)
{
    auto cls = PyFinder::py_init_findertemplate<TracemoeFinder, name>(m);
    cls.def("reserve", &TracemoeFinder::reserve);
    cls.def("get_results", [](TracemoeFinder& f, std::string query, float sim, int max_res, bool clear)
        {
            return f.get_results(TracemoeDesc{ query }, sim, max_res, clear);
        }, pybind11::arg("query"), pybind11::arg("min_similarity"), pybind11::arg("max_results"), pybind11::arg("clear_results"));
    cls.def("__getitem__", [](TracemoeFinder& f, size_t index)
        {
            auto item = f.at(index);
            // If I use array_length as a argument directly it ends up as an
            // undefined symbol. No idea why though...
            constexpr auto sz = TracemoeDesc::array_length;
            return pybind11::bytes(reinterpret_cast<char*>(&item.coeffs[0]), sz);
        });

    cls.def("build_ann", &TracemoeFinder::build_ann);
    cls.def("load_ann", &TracemoeFinder::load_ann);
    cls.def("search_ann", [](TracemoeFinder& f, std::string query, int results)
        {
            pybind11::gil_scoped_release release;
            return f.search_ann(query, results);
        }
    );
    /*cls.def("get_new", [](TracemoeFinder& f, std::string query, float sim, int max_res)
        {
            return f.get_new(TracemoeDesc{ query }, sim, max_res);
        });*/
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
