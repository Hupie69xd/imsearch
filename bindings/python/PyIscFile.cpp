// PyIscFileHelper.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/python/PyIscFile.hpp>

#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <util/IscFileHelper.hpp>
#include <util/TraceConvert.hpp>


namespace imsearch {


template <typename... Args>
using overload_cast_ = pybind11::detail::overload_cast_impl<Args...>;


void PyIscFile::py_init_iscfile(pybind11::module& m)
{
    auto& cls2 = pybind11::class_<IscFileHelper>(m, "IscFile")
        .def(pybind11::init<>())
        .def("add_index", &IscFileHelper::add_index)
        .def("set_fps", &IscFileHelper::set_fps)
        .def("set_name", &IscFileHelper::set_name)
        .def("set_zip_id", &IscFileHelper::set_zip_id)
        .def("to_file", [](IscFileHelper& f)
            {
                const auto result = f.to_file();
                return pybind11::bytes(reinterpret_cast<const char*>(&result[0]), result.size());
            });
}

void PyIscFile::py_init_iscindex(pybind11::module& m)
{
    using vec32 = std::vector<uint32_t>;
    using vec8 = std::vector<uint8_t>;

    auto& cls1 = pybind11::class_<IscIndexHelper>(m, "IscIndex")
        .def(pybind11::init<>())
        .def("add_hashdata", [](IscIndexHelper& f, const vec8& vect)
                {
                     f.add_hashdata(imsearch::BytesView(vect));
                })
        .def("add_imageindex", pybind11::overload_cast<const vec32&>(&IscIndexHelper::add_imageindex))
        .def("add_timeindex", pybind11::overload_cast<const vec32&>(&IscIndexHelper::add_timeindex))
        .def("add_title", &IscIndexHelper::add_title)
        .def("set_descriptor_length", &IscIndexHelper::set_descriptor_length)
        .def("set_type", &IscIndexHelper::set_type);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
