// PySimpleFinder.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <pybind11/pybind11.h>

#include <descriptors/SimpleHash.hpp>
#include <bindings/python/PyFinder.hpp>
#include <bindings/python/PySimpleFinder.hpp>
#include <SimpleFinder.hpp>


namespace imsearch {


constexpr char name[] = "SimpleFinder";


void PySimpleFinder::py_init_simplehash_finder(pybind11::module& m)
{
    auto cls = PyFinder::py_init_findertemplate<SimpleFinder, name>(m);
    cls.def("get_results", [](SimpleFinder& f, uint64_t query, float sim, int max_res, bool clear)
        {
            return f.get_results(SimpleHash{ query }, sim, max_res, clear);
        });

    cls.def("__getitem__", [](SimpleFinder& f, size_t index)
        {
            auto item = f.at(index);
            return item.hash;
        });
    cls.def("build_ann", &SimpleFinder::build_ann);
    cls.def("load_ann", &SimpleFinder::load_ann);
    cls.def("search_ann", [](SimpleFinder& f, uint64_t query, int results)
        {
            pybind11::gil_scoped_release release;
            return f.search_ann(query, results);
        }
    );
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
