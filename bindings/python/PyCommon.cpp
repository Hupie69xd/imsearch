// py_common.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <bindings/python/PyCommon.hpp>

#include <string>

#include <pybind11/stl.h>

#include <util/BytesView.hpp>
#include <util/Lz4File.hpp>


namespace imsearch {


std::vector<int> PyCommon::make_timeindex(const std::vector<uint64_t>& hashes, uint64_t separator)
{
    int end = 0;
    std::vector<int> out{};

    for (auto& hash : hashes)
    {
        if (hash == separator)
            out.push_back(end);
        else
            ++end;
    }

    if (out.size() == 0 || out.back() != end)
        out.push_back(end);

    return out;
}


pybind11::bytes PyCommon::compress_lz4(const std::string& data, const int comp_level)
{
    auto ret = Lz4File::compress(imsearch::BytesView(data), comp_level);
    return { reinterpret_cast<char*>(ret.data()), ret.size() };
}


pybind11::bytes PyCommon::decompress_lz4(const std::string& data)
{
    auto ret = Lz4File::decompress(imsearch::BytesView(data));
    return { reinterpret_cast<char*>(ret.data()), ret.size() };
}


void PyCommon::py_init_py_common(pybind11::module& m)
{
    m.def("make_timeindex", &make_timeindex);
    m.def("compress_lz4", &compress_lz4);
    m.def("decompress_lz4", &decompress_lz4);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
