// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <util/BytesView.hpp>
#include <util/IscFileHelper.hpp>

#include <string>
#include <vector>

#include <gtest/gtest.h>


namespace imsearch {


class IscFileHelperTest : public ::testing::Test {
 protected:
  // void SetUp() override {}

  // void TearDown() override {}

  IscFileHelper isc;
};

TEST_F(IscFileHelperTest, BuildEmpty)
{
    auto bytes = isc.to_file();
}

TEST_F(IscFileHelperTest, BuildNoIndex)
{
    const std::string str{ "testing" };
    isc.set_name(str);
    isc.set_zip_id(str);
    isc.set_fps(1.0f);

    auto bytes = isc.to_file();
}

TEST_F(IscFileHelperTest, BuildNoFields)
{
    auto index = IscIndexHelper{};
    isc.add_index(index);
    auto bytes = isc.to_file();
}

TEST_F(IscFileHelperTest, SetIndexFields)
{
    auto index = IscIndexHelper{};
    const std::string str { "test string" };
    const std::vector<unsigned int> vect { 2701795561, 4140544864, 1862892854, 286406940, 3485519701};
    const std::vector<unsigned char> vect_byte { 193, 57, 218, 188, 141, 248, 73, 128, 73, 14, 245, 128};

    index.set_descriptor_length(4);
    index.set_type(12);
    index.add_title(str, 0);
    index.add_timeindex(vect);
    index.add_imageindex(vect);
    index.add_hashdata(imsearch::BytesView(vect_byte));
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
