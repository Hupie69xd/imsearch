// Lz4File.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <stdexcept>
#include <string>
#include <vector>

#include <util/BytesView.hpp>


namespace imsearch {


struct Lz4File
{
    static size_t get_uncompressed_size(const imsearch::BytesView data_in);

    static std::vector<uint8_t> compress(const imsearch::BytesView data_in,
                                         const int comp_level = 9);

    static std::vector<uint8_t> decompress(const imsearch::BytesView data_in);

private:

    static void throw_if_err(const size_t err, const std::string why);
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
