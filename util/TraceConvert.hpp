// TraceConvert.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <string>
#include <utility>
#include <vector>

#include <util/BytesView.hpp>


namespace imsearch {


struct TraceConvert
{
    using _traceVecPair = std::vector<std::pair<float, std::string>>;


    static std::vector<uint8_t> trace_to_iscf(const imsearch::BytesView view,
                                              const std::string& input_file);

    static _traceVecPair load_from_data(imsearch::BytesView data);

    static std::vector<uint32_t> make_timeindex(const std::vector<float>& timestamps);

    static void unpack_pair_vec(const _traceVecPair& input, std::vector<float>& out_timestamps, std::vector<uint8_t>& out_hashes);

    static void unpack_str(const std::string&, std::vector<uint8_t>*) noexcept;

    static float get_timestamp(const std::string& id_field);

    static _traceVecPair filter_duplicates(const _traceVecPair& in);
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
