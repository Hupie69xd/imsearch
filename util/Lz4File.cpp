// Lz4File.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <util/Lz4File.hpp>

#include <string>

#include <lz4frame.h>


namespace imsearch {


size_t Lz4File::get_uncompressed_size(const imsearch::BytesView data_in)
{
    auto frame_info = LZ4F_frameInfo_t(LZ4F_INIT_FRAMEINFO);
    LZ4F_decompressionContext_t context = nullptr;
    size_t err = LZ4F_createDecompressionContext(&context, LZ4F_getVersion());
    throw_if_err(err, "Can't create context.");

    size_t sz = data_in.size;
    err = LZ4F_getFrameInfo(context, &frame_info, data_in.ptr, &sz);
    LZ4F_freeDecompressionContext(context);

    throw_if_err(err, "Can't get frame info.");

    if (frame_info.contentSize <= 0)
        throw std::runtime_error("No content size");

    return frame_info.contentSize;
}

std::vector<uint8_t> Lz4File::compress(const imsearch::BytesView data_in,
                                       const int comp_level)
{
    auto frame_info = LZ4F_frameInfo_t(LZ4F_INIT_FRAMEINFO);
    frame_info.contentSize = data_in.size;
    frame_info.contentChecksumFlag = LZ4F_contentChecksumEnabled;

    auto prefs = LZ4F_preferences_t(LZ4F_INIT_PREFERENCES);
    prefs.frameInfo = frame_info;
    prefs.compressionLevel = comp_level;
    prefs.autoFlush = 1;

    const size_t min_size = LZ4F_compressFrameBound(data_in.size, &prefs);

    auto data_out = std::vector<uint8_t>(min_size, 0);

    size_t c_size = LZ4F_compressFrame(&data_out[0], data_out.size(), data_in.ptr, data_in.size, &prefs);

    throw_if_err(c_size, "Can't compress data.");

    data_out.resize(c_size);

    return data_out;
}

std::vector<uint8_t> Lz4File::decompress(const imsearch::BytesView data_in)
{
    const size_t content_size = get_uncompressed_size(data_in);
    auto data_out = std::vector<uint8_t>(content_size, 0);

    LZ4F_decompressOptions_t opt{};
    opt.stableDst = 1;
    size_t out_size = data_out.size();

    LZ4F_decompressionContext_t context = nullptr;
    size_t err = LZ4F_createDecompressionContext(&context, LZ4F_getVersion());
    size_t sz = data_in.size;
    err = LZ4F_decompress(context, &data_out[0], &out_size, data_in.ptr, &sz, &opt);

    LZ4F_freeDecompressionContext(context);
    throw_if_err(err, "Can't decompress data.");

    data_out.resize(out_size);

    return data_out;
}

void Lz4File::throw_if_err(const size_t err, const std::string why)
{
    if (LZ4F_isError(err))
    {
        std::string err_message = LZ4F_getErrorName(err);
        throw std::runtime_error(why + " " + err_message);
    }
    else
        return;
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
