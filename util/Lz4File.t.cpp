// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <util/BytesView.hpp>
#include <util/Lz4File.hpp>

#include <gtest/gtest.h>

#include <algorithm>
#include <climits>
#include <functional>
#include <random>
#include <vector>


namespace imsearch {


using random_bytes_engine = std::independent_bits_engine<
    std::default_random_engine, CHAR_BIT, unsigned short>;

TEST(Lz4FileTest, AllFunction)
{
    constexpr int size  = 7 * 1024 * 1024;

    random_bytes_engine rbe{};
    std::vector<uint8_t> data(size, 0);

    std::generate(std::begin(data), std::end(data), std::ref(rbe));

    const auto compressed_data = Lz4File::compress(imsearch::BytesView(data), 3);

    auto decomp_size = Lz4File::get_uncompressed_size(imsearch::BytesView(compressed_data));
    EXPECT_EQ(decomp_size, size);

    auto decompresed_data2 = Lz4File::decompress(imsearch::BytesView(compressed_data));
    EXPECT_EQ(data, decompresed_data2);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
