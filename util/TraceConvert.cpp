// TraceConvert.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <util/TraceConvert.hpp>

#include <algorithm>
#include <array>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <base64.h>
#include <pugixml.hpp>

#include <descriptors/TracemoeDesc.hpp>
#include <IscFile_generated.hpp>
#include <util/BytesView.hpp>
#include <util/Lz4File.hpp>


namespace imsearch {


std::vector<uint8_t> TraceConvert::trace_to_iscf(const imsearch::BytesView view,
                                                 const std::string& input_file)
{
    auto data = load_from_data(view);

    std::sort(data.begin(), data.end()); // tracemoe's files are not necessarly in order
    data = filter_duplicates(data);

    std::vector<float> x{};
    std::vector<uint8_t> hashes{};

    unpack_pair_vec(data, x, hashes);

    const auto timeindex = make_timeindex(x);
    const std::vector<uint32_t> imageindex{ static_cast<uint32_t>(data.size()), 0 }; // not used

    // construct
    flatbuffers::FlatBufferBuilder builder{};

    const unsigned char type = 2;

    const auto xindex = imsearch::CreateIscIndexDirect(builder, data.size(), type, &timeindex, &imageindex, nullptr, &hashes);
    std::vector<flatbuffers::Offset<imsearch::IscIndex>> indecies;
    indecies.push_back(xindex);
    const auto cf_indecies = builder.CreateVector(indecies);

    const auto name = builder.CreateString(input_file);
    const auto zip_id = builder.CreateString("T.moe");

    const auto finished_cfile = imsearch::CreateIscFile(builder, name, zip_id, 1.0f, cf_indecies);
    builder.Finish(finished_cfile, "ISCF");

    // write and compress data
    uint8_t* buf = builder.GetBufferPointer();
    const auto buf_size = builder.GetSize();

    return Lz4File::compress({ buf, buf_size }, 9);
}


TraceConvert::_traceVecPair TraceConvert::filter_duplicates(const _traceVecPair& in)
{
    auto output = _traceVecPair();
    output.reserve(in.size());
    std::string last = "";
    for (auto& item: in)
    {
        if (std::get<1>(item) != last)
        {
            output.push_back(item);
            last = std::get<1>(item);
        }
    }

    return output;
}


std::vector<uint32_t> TraceConvert::make_timeindex(const std::vector<float>& timestamps)
{
    std::vector<uint32_t> index{};
    float next{ 1.0f };

    for (uint32_t i = 0; i < timestamps.size(); ++i)
    {
        while (next < timestamps[i])
        {
            index.push_back(i);
            ++next;
        }
    }
    // empty or incomplete index
    if (index.size() == 0 ||
        index.back() != timestamps.size())
    {
        index.push_back(timestamps.size());
    }
    return index;
}


void TraceConvert::unpack_pair_vec(const _traceVecPair& input, std::vector<float>& output_timestamps, std::vector<uint8_t>& output_hashes)
{
    output_timestamps.reserve(input.size());
    output_hashes.reserve(input.size() * TracemoeDesc::array_length);

    for (const auto& item : input)
    {
        output_timestamps.push_back(std::get<0>(item));
        unpack_str(std::get<1>(item), &output_hashes);
    }
}


void TraceConvert::unpack_str(const std::string& item, std::vector<uint8_t>* out) noexcept
{
    const TracemoeDesc desc{ item };
    std::copy(desc.coeffs.cbegin(), desc.coeffs.cend(), std::back_inserter(*out));
}


TraceConvert::_traceVecPair TraceConvert::load_from_data(imsearch::BytesView view)
{
    pugi::xml_document doc;
    const pugi::xml_parse_result result = doc.load_buffer(view.ptr, view.size);

    if (!result)
        throw std::invalid_argument("Can't open file.");

    const pugi::xml_node add = doc.child("add");

    _traceVecPair out{};
    for (const pugi::xml_node doc : add.children())
    {
        float timestamp{ -1.0f };
        std::string hash{};
        for (const pugi::xml_node field : doc.children())
        {
            const std::string field_name = field.attribute("name").as_string();

            if (field_name == "id")
            {
                timestamp = get_timestamp(field.text().as_string());
            }
            else if (field_name == "cl_hi")
            {
                hash = base64_decode(field.text().as_string());
            }
        }
        out.push_back(std::make_pair(timestamp, hash));
    }

    return out;
}


float TraceConvert::get_timestamp(const std::string& id_field)
/*
* There are three possible ways the timestamp is placed in the id_field
* 1. On it's own e.g. : 12.34
* 2. After the filename of the file it was extracted from e.g. : 2021-01-24/Filename of show.mp4?t=12.34
* 3. Third one is more of a exception where the value is "Undefined"
*/
{
    const auto pos = id_field.find("?t=");

    if (pos != std::string::npos)
        return std::stof(id_field.substr(pos + 3, std::string::npos));

    return static_cast<float>(std::atof(id_field.c_str()));
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
