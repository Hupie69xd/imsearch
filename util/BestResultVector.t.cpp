// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <util/BestResultVector.hpp>

#include <gtest/gtest.h>


namespace imsearch {


class BestVecTest : public ::testing::Test {
 protected:
  // void SetUp() override {}

  // void TearDown() override {}

  BestResultVector<int> vec;
};

TEST_F(BestVecTest, IsEmpty)
{
    EXPECT_EQ(vec.get().size(), 0);

    vec.set_params(20, 1000);
    EXPECT_EQ(vec.get().size(), 0);

    vec.add(1, 1);
    vec.clear();
    EXPECT_EQ(vec.get().size(), 0);
}


TEST_F(BestVecTest, AddChecks)
{
    constexpr int distance = 5u;
    constexpr int distance2 = 15u;

    vec.add(4, distance);

    auto res = vec.get();
    ASSERT_EQ(res.size(), 1);
    EXPECT_EQ(res[0], 4);

    vec.clear();
    vec.set_params(10, distance2);
    for (int i = 0; i < 10; ++i)
    {
        vec.add(i, i);
    }
    res = vec.get();

    ASSERT_EQ(res.size(), 10);
    EXPECT_EQ(res[9], 9);
    EXPECT_EQ(vec.worst(), res[9]);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
