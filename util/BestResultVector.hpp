// BestResultVector.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <algorithm>
#include <cstdint>
#include <utility>
#include <vector>


namespace imsearch {


template<typename T>
class BestResultVector
{
    using _pair = std::pair<int, T>;


    std::vector<_pair> list;
    uint64_t _worst;
    int _max_items;


    void insert(T item, int distance)
    {
        const auto it = std::find_if(list.begin(), list.end(),
            [distance](const auto& item) noexcept { return std::get<0>(item) > distance; });

        // even if it is list.end() that still is correct, since insert at end() is the same as push_back
        list.insert(it, std::make_pair(distance, item));
    }

public:

    BestResultVector() :
        _worst(UINT64_MAX),
        _max_items(50)
    {
        list.reserve(_max_items);
    }


    BestResultVector(int max_items, int threshold) :
        _worst(threshold),
        _max_items(max_items)
    {
        list.reserve(max_items);
    }


    void add(T item, int distance)
    {
        if (list.size() == _max_items)
        {
            list.pop_back();
        }
        if (_max_items == list.size() + 1)
        {
            int last;
            if (list.size() == 0)
                last = 0;
            else
                last = std::get<0>(list[_max_items - 2]);

            _worst = std::max(distance, last);
        }

        insert(item, distance);
    }


    void clear()
    {
        list.clear();
        _worst = UINT64_MAX;
    }


    std::vector<T> get() const
    {
        auto out = std::vector<T>();
        out.reserve(list.size());
        for (auto item : list)
            out.push_back(std::get<1>(item));

        return out;
    }


    void set_params(int max_items, int threshold)
    {
        _worst = threshold;
        _max_items = max_items;
        list.reserve(max_items);
    }


    uint64_t worst() const noexcept
    {
        return _worst;
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
