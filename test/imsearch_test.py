#!/bin/env python3
# imsearch_test.py
# Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.


# This file should call all exported functions, mainly to test if
# they're callable from python. Most function have already been tested on the c++ side.

import unittest

import build.pyimsearch as ims


def read_file(fname, mode='r'):
    coding = "utf-8"
    if 'b' in mode:
        coding = None

    with open(fname, mode, encoding=coding) as file:
        data = file.read()
    return data


class SimpleFinderTestCase(unittest.TestCase):
    def setUp(self):
        self.finder = ims.SimpleFinder()
        self.query = 0

    def test_adding_plugins(self):
        self.finder.add_plugin(ims.ImagePlugin())
        self.finder.add_plugin(ims.NamePlugin())
        self.finder.add_plugin(ims.TimePlugin())

    def test_clearing(self):
        self.finder.clear(False)
        self.finder.clear(True)

    def test_get_results(self):
        self.assertEqual(
            self.finder.get_results(self.query, .9, 5, True), [], "Expected empty list")

    def test_load_iscfile(self):
        data = read_file("test/Kakushigoto - 01.isc", 'rb')
        self.assertEqual(self.finder.load_cfile(data, 255), 0, "Loads the wrong index.")


class TraceFinderTestCase(unittest.TestCase):
    def setUp(self):
        self.finder = ims.TraceFinder()
        self.query = "0" * 33


class PluginTestCase(unittest.TestCase):
    def setUp(self):
        self.plugins = [ims.ImagePlugin(), ims.NamePlugin(), ims.TimePlugin()]
        self.finder = ims.TraceFinder()
        data = read_file("test/Kakushigoto - 01.isc", 'rb')
        for i in self.plugins:
            self.finder.add_plugin(i)
        self.finder.load_cfile(data, 2)

    def test_plugins_clear(self):
        for i in self.plugins:
            i.clear()

    def test_plugins_results(self):
        for i in self.plugins:
            i.retrive_result(5000, {})

class TestUtil(unittest.TestCase):

    def test_make_img_index(self):
        self.assertEqual(ims.make_img_index(list(range(15))), [15, 0])

    def test_make_timeindex(self):
        self.assertEqual(ims.util.make_timeindex(list(range(15)), 10), [10, 14])

    def test_IscIndex(self):
        ii = ims.util.IscIndex()
        ii.add_hashdata(list(b"123543y4"))
        ii.add_imageindex([15, 0])
        ii.add_timeindex([1, 2, 31])
        ii.add_title("Hello", 55)
        ii.set_descriptor_length(8)
        ii.set_type(255)

    def test_IscFile(self):
        ic = ims.util.IscFile()
        ic.add_index(ims.util.IscIndex())
        ic.set_fps(1.1)
        ic.set_name("World")
        ic.set_zip_id("id")
        _ = ic.to_file()

    def test_lz4(self):
        in_data = b"Hello world.jpg"
        dd = ims.util.compress_lz4(in_data, 9)
        self.assertEqual(ims.util.decompress_lz4(dd), in_data, "Bytes not the same.")

if __name__ == '__main__':
    unittest.main()

""" ImSearch
   Copyright (C) 2022  Hupie (hupiew[at]gmail.com)

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. """
   