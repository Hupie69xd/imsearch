// TimePlugin.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <string>
#include <vector>

#include <plugins/FinderPlugin.hpp>


namespace imsearch {


struct TimeStruct
{
    const uint32_t range_end; // range_start being implicit either 0 or range_end of the last struct
    const uint32_t data_vect_offset;
    const uint32_t position_offset;
};

class TimePlugin :
    public FinderPlugin
{
protected:
    uint32_t index_loc{ 0 };
    std::vector<TimeStruct> struct_vect{};
    std::vector<uint32_t> data_vect{}; /* new */

public:
    static constexpr char plugin_name[] = "TimePlugin";

    dict_t& retrive_result(const uint64_t position, dict_t& result_dict) const override;
    void load(const imsearch::IscFile*, const imsearch::IscIndex*) override;
    void clear() override;

    static std::string int_to_time(int);
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
