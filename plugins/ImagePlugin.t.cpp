// ImagePlugin.t.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <plugins/ImagePlugin.hpp>

#include <gtest/gtest.h>


namespace imsearch {


class ImagePluginTest : public ::testing::Test {
 protected:
  // void SetUp() override {}

  // void TearDown() override {}
};


TEST_F(ImagePluginTest, MakeIndexTest)
{
    const std::vector<uint32_t> vv {1000, 0};
    auto vec = std::vector<uint32_t>(1000);
    for (int i = 0; i < vec.size(); ++i)
    {
        vec[i] = i;
    }
    EXPECT_EQ(ImagePlugin::make_index(vec), vv);

    const std::vector<uint32_t> vv2 {500, 0, 1000, 500};
    for (int i = 500; i < vec.size(); ++i)
    {
        vec[i] = i + 500;
    }
    EXPECT_EQ(ImagePlugin::make_index(vec), vv2);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
