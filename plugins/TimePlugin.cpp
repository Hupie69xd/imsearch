// TimePlugin.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <plugins/TimePlugin.hpp>

#include <algorithm>
#include <stdexcept>
#include <string>


namespace imsearch {


void TimePlugin::load(const imsearch::IscFile* cfile, const imsearch::IscIndex* index)
{
    //IndexStruct<T> tmp_strct = { (indexcount - 1), (init_end + indexcount), indexcount, init_name, init_index };
    const uint32_t length = index->length();
    const auto end = index_loc + length;

    struct_vect.push_back({ end, static_cast<uint32_t>(data_vect.size()), index_loc });

    const auto j = index->timeindex();
    std::copy(j->cbegin(), j->cend(), std::back_inserter(data_vect)); // d_vect

    index_loc = end;
}


void TimePlugin::clear()
{
    index_loc = 0;
    struct_vect.clear();
    data_vect.clear();
}


FinderPlugin::dict_t& TimePlugin::retrive_result(const uint64_t position, dict_t& result_dict) const
{
    uint32_t start_at{};
    uint64_t local_position{};
    for (const auto& item : struct_vect)
    {
        if (position < item.range_end)
        {
            start_at = item.data_vect_offset;
            local_position = position - item.position_offset;
            break;
        }
    }

    const uint32_t* last = &data_vect[start_at];
    for (uint64_t i{ start_at }; i < data_vect.size(); ++i)
    {
        if (!(*last <= data_vect[i]))
            break;
        else
            last = &data_vect[i];

        if (local_position < data_vect[i])
        {
            result_dict["timecode"] = int_to_time(i - start_at);
            return result_dict;
        }
    }
    throw std::out_of_range("Number not in TimePlugin.");
}


std::string TimePlugin::int_to_time(int in_seconds)
{
    // looking good this function
    std::string s{};
    const int seconds = in_seconds % 60;
    const int hours = in_seconds / 3600;
    const int remainder = in_seconds % 3600;
    const int minutes = remainder / 60;

    if (hours < 10)
        s.append("0");
    s.append(std::to_string(hours));
    s.append("h");

    if (minutes < 10)
        s.append("0");
    s.append(std::to_string(minutes));
    s.append("m");

    if (seconds < 10)
        s.append("0");
    s.append(std::to_string(seconds));
    s.append("s");

    return s;
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
