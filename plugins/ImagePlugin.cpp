// ImagePlugin.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <plugins/ImagePlugin.hpp>

#include <stdexcept>
#include <utility>


namespace imsearch {


FinderPlugin::dict_t& ImagePlugin::retrive_result(const uint64_t position, dict_t& result_dict) const
{
    uint32_t start_at{};
    uint64_t local_position{};
    for (const auto& item : struct_vect)
    {
        if (position < item.range_end)
        {
            start_at = item.data_vect_offset;
            local_position = position - item.position_offset;
            break;
        }
    }

    for (uint64_t i{ start_at }; i < data_vect.size(); ++i) // while last < current
    {
        if (local_position < std::get<0>(data_vect[i]))
        {
            result_dict["img_id"] = std::to_string(local_position + std::get<1>(data_vect[i]));
            return result_dict;
        }

        if ( !(std::get<0>(data_vect[i]) <= std::get<0>(data_vect[i + 1])) ) break;
    }
    throw std::out_of_range("Number not in ImagePlugin.");
}


void ImagePlugin::load(const imsearch::IscFile* cfile, const imsearch::IscIndex* index)
{
    const uint32_t length = index->length();
    auto const end = index_loc + length;

    struct_vect.push_back({ end, static_cast<uint32_t>(data_vect.size()), index_loc });

    const auto j = index->imageindex();
    for (uint32_t i = 0; i < j->size(); i += 2)
        data_vect.emplace_back(j->Get(i), j->Get(i + 1));

    index_loc = end;
}


void ImagePlugin::clear()
{
    index_loc = 0;
    struct_vect.clear();
    data_vect.clear();
}


std::vector<uint32_t> ImagePlugin::make_index(const std::vector<uint32_t> &index)
{
    uint32_t i{};
    auto last = index[0];
    uint32_t offset = last;
    std::vector<uint32_t> indx_out{};

    while (i < index.size())
    {
        if (index[i] != last)
        {
            indx_out.push_back(i);
            indx_out.push_back(offset);
            offset += index[i] - last;
            last = index[i];
        }
        ++last;
        ++i;
    }

    if (indx_out.size() == 0 || offset != i)
    {
        indx_out.push_back(i);
        indx_out.push_back(offset);
    }
    return indx_out;
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
