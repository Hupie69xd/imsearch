// TimePlugin.t.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <plugins/TimePlugin.hpp>

#include <gtest/gtest.h>


namespace imsearch {


class TimePluginTest : public ::testing::Test {
 protected:
  // void SetUp() override {}

  // void TearDown() override {}

  // TimePlugin plugin{};
};


TEST_F(TimePluginTest, IntToTimeTest)
{
    EXPECT_EQ(TimePlugin::int_to_time(0), "00h00m00s");

    EXPECT_EQ(TimePlugin::int_to_time(60), "00h01m00s");
    EXPECT_EQ(TimePlugin::int_to_time(61), "00h01m01s");

    EXPECT_EQ(TimePlugin::int_to_time(3599), "00h59m59s");
    EXPECT_EQ(TimePlugin::int_to_time(3600), "01h00m00s");
    EXPECT_EQ(TimePlugin::int_to_time(3601), "01h00m01s");
    EXPECT_EQ(TimePlugin::int_to_time(3666), "01h01m06s");
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
