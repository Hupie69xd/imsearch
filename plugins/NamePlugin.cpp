// NamePlugin.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <plugins/NamePlugin.hpp>

#include <stdexcept>


namespace imsearch {


FinderPlugin::dict_t& NamePlugin::retrive_result(const uint64_t position, dict_t& result_dict) const
{
    for (int i{ 0 }; i < ranges.size(); ++i)
    {
        if (position < ranges[i])
        {
            result_dict["name"] = std::get<1>(name_vect[i]);
            result_dict["zip_id"] = std::get<0>(name_vect[i]);

            for (int j{ 0 }; j < sub_ranges.size(); ++j)
            {
                if (position < sub_ranges[j])
                {
                    const std::string& title = sub_name_vect[j];
                    result_dict["name"] += title;
                    break;
                }
            }
            return result_dict;
        }
    }

    throw std::out_of_range("Number not in NamePlugin.");
}



void NamePlugin::load(const imsearch::IscFile* cfile, const imsearch::IscIndex* index)
{
    add_sub_names(cfile, index);
    const uint32_t length = index->length();
    index_loc += length;
    ranges.push_back(index_loc);

    const std::string zip_id = cfile->zip_id()->str();
    const std::string name = cfile->name()->str();
    name_vect.emplace_back(zip_id, name);
}


void NamePlugin::add_sub_names(const imsearch::IscFile* cfile, const imsearch::IscIndex* index)
{
    auto titles = index->titles();
    if (titles == nullptr)
    {
        sub_index += index->length();
        sub_ranges.push_back(sub_index);
        sub_name_vect.emplace_back("");
    }
    else
    {
        for (auto item : *titles)
        {
            sub_index += item->offset();
            sub_ranges.push_back(sub_index);
            sub_name_vect.push_back(item->title_str()->str());
        }
    }
}


void NamePlugin::clear()
{
    index_loc = 0;
    ranges.clear();
    name_vect.clear();
    sub_ranges.clear();
    sub_name_vect.clear();
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
