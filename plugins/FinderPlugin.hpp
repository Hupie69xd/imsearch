// FinderPlugin.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <string>
#include <unordered_map>

#include <IscFile_generated.hpp>


namespace imsearch {


class FinderPlugin
{
public:
    using dict_t = std::unordered_map<std::string, std::string>;
    static constexpr char plugin_name[] = "FinderPlugin";

    virtual ~FinderPlugin() = default;
    /* Destructor that does nothing. */

    virtual void load(const imsearch::IscFile*, const imsearch::IscIndex*) {}
    /*  */

    virtual void clear() {}
    /* Clears all data from the plugin. Should behave as if it was just contructed. */

    virtual dict_t& retrive_result(const uint64_t position, dict_t& result_dict) const
    {
        return result_dict;
    }
    /*  */
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
