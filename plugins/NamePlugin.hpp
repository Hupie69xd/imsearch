// NamePlugin.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <string>
#include <utility>
#include <vector>

#include <plugins/FinderPlugin.hpp>


namespace imsearch {


using names = std::pair<std::string, std::string>; // zip_id and (file)name

class NamePlugin :
    public FinderPlugin
{
protected:
    uint32_t index_loc{ 0 };
    uint64_t sub_index{ 0 };
    std::vector<uint64_t> ranges{};
    std::vector<names> name_vect{};

    std::vector<uint64_t> sub_ranges{};
    std::vector<std::string> sub_name_vect{};

    void add_sub_names(const imsearch::IscFile*, const imsearch::IscIndex*);

public:
    static constexpr char plugin_name[] = "NamePlugin";

    dict_t& retrive_result(const uint64_t position, dict_t& result_dict) const override;
    void load(const imsearch::IscFile*, const imsearch::IscIndex*) override;
    void clear() override;
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
