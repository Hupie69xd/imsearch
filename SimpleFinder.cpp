// SimpleFinder.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <SimpleFinder.hpp>


namespace imsearch {


void SimpleFinder::build_ann(int k_trees, int n_jobs)
{
    constexpr char fname[] = "SimpleF_3.ann";

    auto ann = AnnoySimple(SimpleHash::array_length);

    char* error;
    if (!ann.on_disk_build(fname, &error))
    {
        std::string err = error;
        free(error);
        throw std::runtime_error(err);
    }

    for (uint32_t i = 0; i < hash_list.size(); ++i)
    {
        const bool ok = ann.add_item(i, &hash_list[i].hash, &error);
        if (!ok) [[unlikely]]
        {
            std::string err = error;
            free(error);
            throw std::runtime_error(err);
        }
    }

    hash_list.clear();
    hash_list.shrink_to_fit();

    if (!ann.build(k_trees, n_jobs, &error))
    {
        std::string err = error;
        free(error);
        throw std::runtime_error(err);
    }

    ann.unload();
}


bool SimpleFinder::load_ann(const std::string& filename)
{
    bool ok = index.load(filename.c_str());
    hash_list.clear();
    hash_list.shrink_to_fit();
    return ok;
}


SimpleFinder::results_t SimpleFinder::search_ann(const uint64_t query,const int max_results)
{
    std::vector<int32_t> r;
    std::vector<uint64_t> d;
    index.get_nns_by_vector(&query, max_results, -1, &r, &d);

    std::vector<FinderPlugin::dict_t> list{};
    for (int i = 0; i < r.size(); ++i)
    {
        FinderPlugin::dict_t map{};
        map["distance"] = std::to_string(d[i]);

        for (auto& plugin : plugins)
        {
            map = plugin.get()->retrive_result(r[i], map);
        }
        list.push_back(std::move(map));
    }

    return list;
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
