// TracemoeFinder.t.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <TracemoeFinder.hpp>

#include <descriptors/TracemoeDesc.hpp>
#include <plugins/ImagePlugin.hpp>
#include <plugins/NamePlugin.hpp>
#include <plugins/TimePlugin.hpp>
#include <util/BytesView.hpp>
#include <util/Lz4File.hpp>

#include <gtest/gtest.h>

#include <fstream>
#include <memory>
#include <stdexcept>
#include <vector>


namespace imsearch {


class FinderTest : public ::testing::Test {
 protected:
  void SetUp() override
  {
    // https://stackoverflow.com/questions/15366319/how-to-read-the-binary-file-in-c
    // the fuck is this bullshit just to read a file.

    std::streampos fileSize;
    std::ifstream file("test/Kakushigoto - 01.isc", std::ios::binary);
    if (!file.good())
        throw std::runtime_error("Unable to open file.");

    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    data.resize(fileSize, 0);
    file.read(reinterpret_cast<char*>(&data[0]), fileSize);
  }

  // void TearDown() override {}

  TracemoeFinder tf{};
  std::vector<uint8_t> data{};
};


TEST_F(FinderTest, LoadTest)
{
    imsearch::BytesView view { data };
    EXPECT_EQ(tf.load_cfile(view, 2), 1);
    EXPECT_EQ(tf.size(), 11143);

    auto uncompressed = Lz4File::decompress(view);
    EXPECT_EQ(tf.load_cfile(imsearch::BytesView(uncompressed), 2), 1);
    EXPECT_EQ(tf.size(), 11143 * 2);
}

TEST_F(FinderTest, PluginTest)
{
    imsearch::BytesView view { data };
    auto imPg = std::make_shared<ImagePlugin>();
    auto nmPg = std::make_shared<NamePlugin>();
    auto tmPg = std::make_shared<TimePlugin>();

    tf.add_plugin(imPg);
    tf.add_plugin(nmPg);
    tf.add_plugin(tmPg);

    tf.load_cfile(view, 2);
    // should throw if it has things loaded
    EXPECT_ANY_THROW(tf.add_plugin(imPg));
}

// This at the same time, kind of tests the plugins.
TEST_F(FinderTest, SearchTest)
{
    imsearch::BytesView view { data };
    auto imPg = std::make_shared<ImagePlugin>();
    auto nmPg = std::make_shared<NamePlugin>();
    auto tmPg = std::make_shared<TimePlugin>();
    tf.add_plugin(imPg);
    tf.add_plugin(nmPg);
    tf.add_plugin(tmPg);

    tf.load_cfile(view, 2);
    const auto& desc = tf.at(4020);
    auto results = tf.get_results(desc, 0.5, 1, true);

    ASSERT_EQ(results.size(), 1);
    auto& res = results[0];

    EXPECT_EQ(res["distance"], "0");
    EXPECT_EQ(res["timecode"], "00h08m33s");
    EXPECT_EQ(res["name"], "Kakushigoto - 01 (BD 1280x720 x264 AAC).mp4");
    EXPECT_EQ(res["zip_id"], "T.moe");
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
