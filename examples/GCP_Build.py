import base64
import glob
import os
import time
import zipfile

from multiprocessing.pool import ThreadPool

#from common import t, read_file, write_file
from pyimsearch import NamePlugin, TimePlugin, TraceFinder

# Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

isc = "trace.isc 2021-10/*.zip"

qq = "CRANExEaDhAPEBAQEBETDg8PEA8PGg8QDw8OJA8OEBAR" # Hero Academia 3 - 07 @ 06m31s
sm = "JAwREwgSDBUSEBETDwwUERASDw0PEQwSDRAMMBMNEhEQ" # Gabriel Dropout 06 @ 20m05s

###### common.py
def t(func, *args, times=1, return_t=False, **kwargs):
    time_list = []
    x = times
    while x:
        tstart = time.perf_counter()
        x -= 1
        tmp = func(*args, **kwargs)
        time_list.append(time.perf_counter() - tstart)

    avg = round(sum(time_list) / times, 6)
    median = time_list[times // 2]
    if times > 1:
        time_list.sort()
        if not (times % 2): # if even
            median = (median + time_list[ (times // 2) - 1 ]) / 2
    min_ = round(min(time_list), 6)
    if return_t:
        return tmp, min_
    total = round(sum(time_list), 6)
    
    print(f"Total: {total}, Min: {min_}")
    return tmp
def read_file(fname, mode='r'):
    if 'w' in mode:
        raise ValueError("Write flag in mode.")
    if 'b' in mode:
        coding = None
    else:
        coding = "utf-8"
    with open(fname, mode, encoding=coding) as file:
        data = file.read()
    return data
def write_file(fname, data, mode='w'):
    if 'r' in mode:
        raise ValueError("Read flag in mode.")
    if 'b' in mode:
        coding = None
    else:
        coding = "utf-8"
    with open(fname, mode, encoding=coding) as file:
        file.write(data)
###### END common.py


def _load(tf, zipname):
    zf = zipfile.ZipFile(zipname)
    
    for j in zf.filelist:
        data = zf.read(j)
        tf.load_cfile(data, 2)


def load_x(tf, x, files, offsets, offset):
    while len(tf) < x:
        begin = len(tf)
        try:
            file = files.pop()
        except IndexError:
            break
        _load(tf, file)
        offsets.append( (begin + offset, len(tf) + offset, file) )

    return len(tf) + offset

def read_offsets(fname):
    with open(fname, 'r', encoding='utf-8') as f:
        data = f.read()

    data = data.split('\n')
    data = [i.split('\t') for i in data]
    output = [ [], ]
    last = ""
    for i in data:
        if len(i) == 1:
            continue
        elif i[2].endswith("TraceF_3.ann"):
            output[-1].append(i[2])
            output.append( [] )
            continue

        output[-1].append(i[2])
    if len(output[-1]) == 0:
        output = output[:-1]
    return output

def init_finders(dir_=".", amount=-1, offsets="offsets.txt"):
    dd = read_offsets(offsets)
    output = []
    if amount == -1:
        amount = len(dd)
    for n, i in enumerate(dd, start=1):
        tf = trace_finder()
        for j in i:
            if j.endswith(".ann"):
                tf.load_ann(j)
                t(tf.search_ann, "0"*33, 1)
                continue
            _load(tf, j)
        output.append(tf)
        if amount == n:
            break
    return output


def do_search(tfs, qq, results):
    def args(tfs, qq, results):
        for i in tfs:
            yield i, qq, results
    def s(tf, qq, a):
        return tf.search_ann(qq, a)
    
    if isinstance(qq, str):
        qq = base64.b64decode(qq)
    with ThreadPool(processes=32) as pool:
        tmp = pool.map(lambda x: s(*x), args(tfs, qq, results))

    r = []
    for i in tmp:
        r.extend(i)
    
    r.sort(key=lambda x: float(x["distance"]))
    return r[:results]


def trace_finder():
    tf = TraceFinder()
    tf.add_plugin(TimePlugin())
    tf.add_plugin(NamePlugin())
    return tf

class AnnBuild:
    def __init__(self, sz, glb='/mnt/disks/annoy/trace.isc 2021-10/*.zip'):
        self.sz = sz
        self.files = glob.glob(glb)
        self.tf = trace_finder()
        self.offsets = []
        self.index_offset = 0
        self.n = 1

    def build_one(self, k_trees=10, threads=-1):
        if self.files:
            new_offset = load_x(self.tf,
                                self.sz,
                                self.files,
                                self.offsets,
                                self.index_offset
                                )
            self.tf.build_ann(k_trees, threads)

            fname = "TraceF_3.ann"
            fname2 = f"{self.n}_" + fname
            self.offsets.append((new_offset, new_offset, fname2))
            os.rename(fname, fname2)

            self.n += 1
            self.tf.clear(False)
            self.index_offset = new_offset

            return True
        else:
            return False

    def write_result(self, fname="offsets.txt"):
        with open(fname, 'a', encoding='utf-8') as file:
            for f, t, fn in self.offsets:
                _ = file.write(f"{f}\t{t}\t{fn}\n")

"""
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
