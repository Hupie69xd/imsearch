import os
import tarfile
import zipfile
import lzma

from pyimsearch.util import convert_trace

# Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

""" Helper function that converts .tar files from the trace.moe database dump,
to files that can be loaded by imsearch.
"""

def tar_to_isc(infile, outpath):
    _, name = os.path.split(infile)
    zipfname = os.path.join(outpath, name[:-3] + "zip")

    if os.path.exists(zipfname):
        return

    tar = tarfile.open(infile, 'r:*')
    zf = zipfile.ZipFile(zipfname, mode='x')

    for file in tar.getmembers():
        if not (file.isfile() and file.name.endswith('.xz')):
            continue

        _, name = os.path.split(file.name)
        name = name[:-7]

        data_buf = tar.extractfile(file)
        # memlimit is 64 mib
        xml = lzma.decompress(data_buf.read(), memlimit=1024*1024*64)

        out_data = convert_trace(xml, name)

        outfile = name + ".isc"
        if out_data.startswith(b"\x1f\x8b"):
            outfile += ".gz"

        zf.writestr(outfile, out_data)

"""
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
