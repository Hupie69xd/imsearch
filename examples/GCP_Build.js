const ims = require("./imsearch-node");
const zipfile = require('node-stream-zip');
const fs = require('fs');

// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.


function do_search(tfs, query, n_results)
{
    var _qq = Buffer.from(atob(query));
    var qq = Uint8Array.from(_qq); // I don't know why a Buffer doesn't work, but it just doesn't.
    var out = new Array();

    for (const tf of tfs)
    {
        var res = tf.searchAnn(qq, n_results);
        out = out.concat(res);
    }
    out.sort((a, b) => {
        var na = Number(a.distance);
        var nb = Number(b.distance);
        if (na < nb) { return 1; }
        if (nb < na) { return -1; }
        return 0;
    })
    return out.slice(n_results);
}


function trace_finder()
{
    var tf = new ims.TracemoeFinder();
    var np = new ims.FinderPlugin();
    ims.initNamePlugin(np);
    var tp = new ims.FinderPlugin();
    ims.initTimePlugin(tp);
    tf.addPlugin(np);
    tf.addPlugin(tp);
    return tf;
}


function init_finders(amount=-1, offsets="offsets.txt")
{
    var dd = read_offsets(offsets);
    var output = new Array();
    var count = 0;
    var cb = () =>
    {
        if (count == amount) { return; }
        count += 1;
        var ann_group = dd.shift();
        var tf = trace_finder();
        output.push(tf);
        _load_cb(tf, ann_group, cb);
    }
    cb();
    return output;
}


function read_offsets(fname)
{
    var data = fs.readFileSync(fname, "utf-8");
    var data1 = data.replace('\r', '').split("\n");
    var output = new Array();
    var inner = new Array();
    for (const item of data1)
    {
        inner.push(item.split('\t')[2])

        if (item.endsWith('.ann'))
        {
            output.push(inner);
            inner = new Array();
            lastWasAnn = true;
        }
    }
    return output;
}


function _load_cb(tf, files, loop_cb)
{
    if (files.length == 0) { return loop_cb(); }
    
    var zipname = files.shift();
    
    if (zipname.endsWith(".ann"))
    {
        tf.loadAnn(zipname);
        console.log(zipname);
        return loop_cb();
    }
    
    const zip = new zipfile({ file: zipname });
    
    zip.on('ready', () => {
        for (const entry of Object.values(zip.entries()))
        {
            const data = zip.entryDataSync(entry);
            tf.loadFile(data, 2);
        }
        zip.close();
        _load_cb(tf, files, loop_cb);
    });
}


module.exports = { init_finders, trace_finder, do_search }

/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/