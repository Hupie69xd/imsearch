// SimpleFinder.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <descriptors/SimpleHash.hpp>
#include <FinderTemplate.hpp>

#define ANNOYLIB_MULTITHREADED_BUILD
#include <annoylib.h>
#include <kissrandom.h>


namespace imsearch {


class SimpleFinder :
    public FinderTemplate<SimpleHash>
{
    using AnnoySimple = Annoy::AnnoyIndex<int32_t, uint64_t, Annoy::Hamming,Annoy::Kiss64Random, Annoy::AnnoyIndexMultiThreadedBuildPolicy>;

    AnnoySimple index{ SimpleHash::array_length };

public:

    void build_ann(int k_trees, int n_jobs);

    bool load_ann(const std::string& filename);

    results_t search_ann(const uint64_t query, const int max_results);
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
