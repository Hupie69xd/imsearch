// imsearch.t.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <imsearch.hpp>

#include <gtest/gtest.h>

#include <stdexcept>
#include <vector>


namespace imsearch {


TEST(PyIndex, IndexingTest)
{
    constexpr int test_size = 10;
    std::vector<int> vec{};


    EXPECT_THROW(python_index(0, vec), std::out_of_range);
    EXPECT_THROW(python_index(-1, vec), std::out_of_range);

    for (int i = 0; i < test_size; ++i)
    {
        vec.push_back(i);
    }

    EXPECT_THROW(python_index(test_size, vec), std::out_of_range);
    EXPECT_THROW(python_index(-(test_size+1), vec), std::out_of_range);

    for (int i = 0; i < test_size; ++i)
    {
        const int rev_index = -(test_size - i);
        EXPECT_EQ(python_index(rev_index, vec), python_index(i, vec));
    }
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
