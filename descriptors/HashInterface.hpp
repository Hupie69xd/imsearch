#pragma once
// HashInterface.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.


namespace imsearch {


template<typename Desc_T, typename arith_T>
struct HashInterface
{
    // Give the max number the distance function can return
    static constexpr arith_T max() noexcept
    {
        Desc_T::max();
    }

    // Equals operator
    bool operator==(const Desc_T& other) const noexcept
    {
        static_cast<Desc_T*>(this)->operator==();
    }

    // tells if a hash is similar, similarity being between 0.0 and 1.0 as how similar a hash must be
    bool is_similar(const Desc_T& other, const float similarity) const noexcept
    {
        return similarity > (distance(other) / this->max());
    }


    /*
    * Converts from a percentage similarity to a distance
    * eg. if max() is equal to 100 and similarity is 0.9 we get 10
    */
    static int from_similarity(const float similarity) noexcept
    {
        auto sq_dis = (1.0 - similarity) * max();
        return static_cast<arith_T>(sq_dis);
    }



    // same as above but takes already computed distance
    bool is_similar(const float distance, const float similarity) const noexcept
    {
        return similarity > (distance / max());
    }

    // calculates the distance between this and other
    // result must be greater than or equal to 0 and less than or equal to max()
    arith_T distance(const Desc_T& other) const noexcept
    {
        static_cast<Desc_T*>(this)->distance();
    }
};

} // namespace imsearch

/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
