// Metrics.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.


#include <descriptors/Metrics.hpp>

#include <exception>


namespace imsearch {


/*
#if defined(__AVX512F__) && defined(__AVX512BW__)
void EuclideanWeighted16::compute32(const __m512i& a, const __m512i& b) noexcept
{
    const __m512i d_32x16 = _mm512_sub_epi16(a, b);
    const __m512i sq_16x32 = _mm512_madd_epi16(d_32x16, d_32x16);
}
#endif

int EuclideanWeighted16::get_distance()
{
#if defined(__AVX512F__) && defined(__AVX512BW__)

    return _mm512_reduce_add_epi32(sum);

#elif defined(__AVX2__)

    const __m256i zero = _mm256_setzero_si256();
    __m256i e = sum;
    e = _mm256_hadd_epi32(e, zero); // 8 -> 4
    e = _mm256_hadd_epi32(e, zero); // 4 -> 2

    const int sc1 = _mm256_extract_epi32(e, 0);
    const int sc2 = _mm256_extract_epi32(e, 4);

    return sc1 + sc2;

#else

    return sum;

#endif
}

int EuclideanWeighted16::distance(const int16_t* a, const int16_t* b, const int length) noexcept
{
    // 30% slower than integrated method atm
    if (a == nullptr || b == nullptr)
    {
        return -1;
    }
    int i = 0;
    int simd_sum = 0;
#if defined(__AVX512F__) && defined(__AVX512BW__)
    __m512i sum = _mm512_setzero_si512();
    for (; (i + 31) < length; i += 32)
    {
        const __m512i a_32x16 = _mm512_loadu_si512(reinterpret_cast<const __m512i*>(&a[i]));
        const __m512i b_32x16 = _mm512_loadu_si512(reinterpret_cast<const __m512i*>(&b[i]));

        const __m512i d_32x16 = _mm512_sub_epi16(a_32x16, b_32x16);

        const __m512i sq_16x32 = _mm512_madd_epi16(d_32x16, d_32x16);

        sum = _mm512_add_epi32(sum, sq_16x32);
    }
    simd_sum = _mm512_reduce_add_epi32(sum);

#elif defined(__AVX2__)

    const __m256i zero = _mm256_setzero_si256();
    __m256i sum = zero;

    for (; (i + 15) < length; i += 16)
    {
        const __m256i a_16x16 = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&a[i]));
        const __m256i b_16x16 = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&b[i]));

        const __m256i d_16x16 = _mm256_sub_epi16(a_16x16, b_16x16);

        const __m256i sq_8x32 = _mm256_madd_epi16(d_16x16, d_16x16); // 8

        sum = _mm256_add_epi32(sum, sq_8x32);
    }
    __m256i e = sum;
    e = _mm256_hadd_epi32(e, zero); // 8 -> 4
    e = _mm256_hadd_epi32(e, zero); // 4 -> 2

    const int sc1 = _mm256_extract_epi32(e, 0);
    const int sc2 = _mm256_extract_epi32(e, 4);

    simd_sum += sc1 + sc2;

#endif

    int scalar_sum{ 0 };
    for (; i < length; ++i)
    {
        int diff = a[i];
        diff -= b[i];
        scalar_sum += (diff * diff);
    }

    return scalar_sum + simd_sum;
}

#if defined(__AVX512F__) && defined(__AVX512BW__)
int EuclideanWeighted16::distance(const __m512i& a_32x16, const __m512i& b_32x16) noexcept
{
    const __m512i d_32x16 = _mm512_sub_epi16(a_32x16, b_32x16);

    const __m512i sq_16x32 = _mm512_madd_epi16(d_32x16, d_32x16);
    
    return _mm512_reduce_add_epi32(sq_16x32);
}
#endif

int EuclideanWeighted16::distance(const __m256i& a_16x16, const __m256i& b_16x16) noexcept
{
    const __m256i d_16x16 = _mm256_sub_epi16(a_16x16, b_16x16);

    const __m256i sq_8x32 = _mm256_madd_epi16(d_16x16, d_16x16); // 8

    __m256i e = sq_8x32;
    const __m256i zero = _mm256_setzero_si256();

    e = _mm256_hadd_epi32(e, zero); // 8 -> 4
    e = _mm256_hadd_epi32(e, zero); // 4 -> 2

    const int sc1 = _mm256_extract_epi32(e, 0);
    const int sc2 = _mm256_extract_epi32(e, 4);

    return sc1 + sc2;
}
*/

} // namespace imsearch
/**
 * Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
 *
 *  This file is part of the ImSearch.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Or alternatively, under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation, either version
 *  3 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU (Lesser) General Public License for more details.
 *
 *  You should have received a copy of the GNU (Lesser) General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
