// Metrics.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <cmath>

#ifdef _MSC_VER
#include <intrin.h>
#define ImSearch_popcnt64 __popcnt64

#elif __GNUC__
#include <x86intrin.h>
#define ImSearch_popcnt64 __builtin_popcountll

#endif

#define ANNOYLIB_MULTITHREADED_BUILD
#include <annoylib.h>


namespace imsearch {


/*
struct _Metric
{
protected:
    template<typename T>
    static int loop(const T& a, const T& b) //, int (*calc)(T, T) )
    {
        int c = 0;
        auto it1 = a.begin();
        auto it2 = b.begin();
        while (it1 != a.end() && it2 != b.end())
        {
            c += calc(it1, it2);
            ++it1;
            ++it2;
        }
        return c;
    }
};
*/

struct EuclideanWeighted16
{
    using AnnoyMetric = Annoy::Euclidean;
    /*
#if defined(__AVX512F__) && defined(__AVX512BW__)
    __m512i sum;
#elif defined(__AVX2__)
    __m256i sum;
#else
    int sum;
#endif


#if defined(__AVX512F__) && defined(__AVX512BW__)
    void compute32(const __m512i& a, const __m512i& b) noexcept;
    void compute32(const int16_t* a, const int16_t* b) noexcept
    {
        const __m512i x = _mm512_loadu_si512(reinterpret_cast<const __m512i*>(a));
        const __m512i y = _mm512_loadu_si512(reinterpret_cast<const __m512i*>(b));
        return compute32(x, y);
    }
    void compute32_8i(const int16_t* a, const int16_t* b) noexcept
    {
        const __m256i x = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(a));
        const __m256i y = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(b));
        const __m512i xx = _mm512_cvtepu8_epi16(x);
        const __m512i yy = _mm512_cvtepu8_epi16(y);

        return compute32(xx, yy);
    }

    void compute16(const int16_t* a, const int16_t* b);
    void compute16_8i(const int16_t* a, const int16_t* b);

    void computex(const int16_t* a, const int16_t* b, const int length);
#endif
    int get_distance();

    static int distance(const int16_t* a, const int16_t* b, const int length) noexcept;

#if defined(__AVX512F__) && defined(__AVX512BW__)
    static int distance(const __m512i& a, const __m512i& b) noexcept;
#endif

#if defined(__AVX2__)
    static int distance(const __m256i& a, const __m256i& b) noexcept;
#endif
    */
};

/*
struct Euclidean
{
protected:
    template<typename T>
    static int distance(const T& a, const T& b) noexcept
    {
        return std::sqrt(EuclideanWeighted16::distance(a, b));
    }
};
*/

struct Hamming
{
    using AnnoyMetric = Annoy::Hamming;

    template<typename T>
    static int distance(const T a, const T b, const int length) noexcept
    {
        int sum = 0;
        for (int i = 0; i < length; ++i)
        {
            sum += ImSearch_popcnt64(a[i] ^ b[i]);
        }
        return static_cast<int>(sum);
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
