// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <descriptors/TracemoeDesc.hpp>

#include <gtest/gtest.h>


namespace imsearch {


class TracemoeDescTest : public ::testing::Test {
protected:
  // void SetUp() override {}

  // void TearDown() override {}

    TracemoeDesc sh1{ "1234567890abcdefghikljmnopqrstuvw" };
    TracemoeDesc sh2{ "wvutsrqponmjlkihgfedcba0987654321" };
};


TEST_F(TracemoeDescTest, SelfDistance)
{
    EXPECT_EQ(sh1.distance(sh1), 0);
    EXPECT_EQ(sh2.distance(sh2), 0);
}

TEST_F(TracemoeDescTest, TestDistance)
{
    constexpr int distance =  110760;
    EXPECT_EQ(sh1.distance(sh2), distance);
    EXPECT_EQ(sh2.distance(sh1), distance);
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
