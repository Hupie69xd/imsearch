// SimpleHash.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <cstdint>

#include <descriptors/Metrics.hpp>


namespace imsearch {


struct SimpleHash
{
    /* A simple hash that gets compared with hamming distance */
    uint64_t hash;

    using metric = Hamming;
    static constexpr int array_length = 1;


    int distance(const SimpleHash& other) const noexcept;


    static int from_similarity(const float similarity) noexcept;


    constexpr static uint64_t max() noexcept
    {
        return sizeof(SimpleHash) * 8;
    }
};

} // namespace imsearch
/**
 * Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
 *
 *  This file is part of the ImSearch.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Or alternatively, under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation, either version
 *  3 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU (Lesser) General Public License for more details.
 *
 *  You should have received a copy of the GNU (Lesser) General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
