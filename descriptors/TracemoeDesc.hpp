// TracemoeDesc.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <array>
#include <cstdint>
#include <string>

#include <descriptors/Metrics.hpp>


namespace imsearch {


struct TracemoeDesc
{
    /*
    * Descriptor constants.
    */
    static constexpr size_t ycoeff_length = 21;
    static constexpr size_t cxcoeff_length = 6;
    static constexpr size_t array_length = ycoeff_length + cxcoeff_length * 2;
    using metric = EuclideanWeighted16;


    /*
    * The image descriptor used by trace.moe,
    * specifically the descriptor used by Lire solr's color layout.
    */
    std::array<int8_t, array_length> coeffs;


    explicit TracemoeDesc(const std::string& data);
    /*
    * Basically a copy of LIRE's setByteArrayRepresentation function
    * Byte one and two might be the length of y and cr/cb coeffs
    */


    int distance(const TracemoeDesc& other) const noexcept;
    /*
    * The distance function is not quite the same as Lire's implementation,
    * but it seems to be only slightly off for a significant speed improvement.
    */


    static int from_similarity(const float similarity) noexcept;
    /*
    * Converts from a percentage similarity to a distance
    * eg. if max() is equal to 100 and similarity is 0.9 we get 10
    */


    static constexpr int max() noexcept;
    /* maximum that can be returned by the distance function */


    bool operator==(const TracemoeDesc& other) const noexcept;
};

} // namespace imsearch
/**
 * Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
 *
 *  This file is part of the ImSearch.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  Or alternatively, under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation, either version
 *  3 of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU (Lesser) General Public License for more details.
 *
 *  You should have received a copy of the GNU (Lesser) General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
