// TracemoeDesc.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include "TracemoeDesc.hpp"

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <immintrin.h>


namespace imsearch {


constexpr std::array<int8_t, TracemoeDesc::array_length> shiftMatrix = {
    1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0,
    2, 1, 1, 0, 0, 0 };


constexpr std::array<int16_t, TracemoeDesc::array_length> multMatrix = {
    2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    2, 1, 1, 1, 1, 1,
    4, 2, 2, 1, 1, 1 };



TracemoeDesc::TracemoeDesc(const std::string& data)
{
    if (data.size() == array_length + 2 &&
        data[0] == ycoeff_length &&
        data[1] == cxcoeff_length)
    {
        std::copy(data.begin() + 2, data.end(), coeffs.begin());
    }
    else if (data.size() == array_length)
        std::copy(data.begin(), data.end(), coeffs.begin());

    else
        throw std::invalid_argument("Couldn't read input string. Maybe you need to b64 decoded your string?");
}


int TracemoeDesc::distance(const TracemoeDesc& other) const noexcept
{
    size_t i = 0;
#if __AVX512F__ && __AVX512BW__
    const __m256i a_32x8 = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&coeffs[i]));
    const __m256i q_32x8 = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&other.coeffs[i]));
    const __m512i weights = _mm512_loadu_si512(reinterpret_cast<const __m512i*>(&multMatrix[i]));

    const __m512i a_32x16 = _mm512_cvtepu8_epi16(a_32x8);
    const __m512i q_32x16 = _mm512_cvtepu8_epi16(q_32x8);

    const __m512i d_32x16 = _mm512_sub_epi16(a_32x16, q_32x16);

    const __m512i weighted = _mm512_mullo_epi16(d_32x16, weights);

    const __m512i sq_16x32 = _mm512_madd_epi16(weighted, d_32x16);

    const int avx_sum = _mm512_reduce_add_epi32(sq_16x32);

    i += 32;
#elif __AVX2__
    const __m256i zero = _mm256_setzero_si256();
    __m256i sum = zero;

    for (; (i + 15) < array_length; i += 16)
    {
        /* Loads 128-bit value. Address p not need be 16-byte aligned.  */
        const __m128i a_16x8 = _mm_loadu_si128(reinterpret_cast<const __m128i*>(&coeffs[i]));
        const __m128i b_16x8 = _mm_loadu_si128(reinterpret_cast<const __m128i*>(&other.coeffs[i]));

        /* Performs packed move with zero - extend on 8 - bit unsigned integers to 16 bit integers. */
        const __m256i a_16x16 = _mm256_cvtepu8_epi16(a_16x8);
        const __m256i b_16x16 = _mm256_cvtepu8_epi16(b_16x8);
        const __m256i d_16x16 = _mm256_sub_epi16(a_16x16, b_16x16);

        /* Weights. */
        const __m256i weights  = _mm256_loadu_si256(reinterpret_cast<const __m256i*>(&multMatrix[i]));
        const __m256i weighted = _mm256_mullo_epi16(weights, d_16x16);

        const __m256i sq_8x32 = _mm256_madd_epi16(weighted, d_16x16); // 8

        sum = _mm256_add_epi32(sum, sq_8x32);
}
    __m256i e = sum;
    e = _mm256_hadd_epi32(e, zero); // 8 -> 4
    e = _mm256_hadd_epi32(e, zero); // 4 -> 2

    const int sc1 = _mm256_extract_epi32(e, 0);
    const int sc2 = _mm256_extract_epi32(e, 4);

    const int avx_sum = sc1 + sc2;
#else

    int scalar_sum{ 0 };
    for (; i < array_length; ++i)
    {
        int diff = coeffs[i];
        diff -= other.coeffs[i];
        scalar_sum += (diff * diff) << shiftMatrix[i];
    }

    return scalar_sum;

    const int avx_sum{ 0 };
#endif

    /* Manual unroll for the last element because the compiler won't */

    int diff = coeffs[i];
    diff -= other.coeffs[i];
    const int squared = diff * diff; // no shift because it is zero

    return avx_sum + squared;
}


int TracemoeDesc::from_similarity(const float similarity) noexcept
{
    auto sq_dis = (1.0 - similarity) * max();
    return static_cast<int>(sq_dis);
}


constexpr int TracemoeDesc::max() noexcept
{
    int squared_distance = 0;
    for (int i = 0; i < array_length; ++i)
        squared_distance += (48 * 48) << shiftMatrix[i];
    return squared_distance;
}


bool TracemoeDesc::operator==(const TracemoeDesc& other) const noexcept
{
    return coeffs == other.coeffs;
}

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
