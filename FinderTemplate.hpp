// Finder_template.hpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#pragma once

#include <algorithm>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include <imsearch.hpp>
#include <IscFile_generated.hpp>
#include <plugins/FinderPlugin.hpp>
#include <util/BestResultVector.hpp>
#include <util/Lz4File.hpp>


namespace imsearch {


template <typename hash_T>
class FinderTemplate
{
protected:
    using res_pair = std::pair<int64_t, size_t>;


    std::vector<hash_T> hash_list{};
    std::vector< std::shared_ptr<FinderPlugin> > plugins{};
    BestResultVector<res_pair> result_vector{};


    void load_iscindex(const imsearch::IscIndex* index)
    {
        const auto dt = index->hash_data();

        if (dt != nullptr)
        {
            constexpr auto desc_sz = sizeof(hash_T);
            const auto sz = dt->size() / desc_sz;
            const auto front_ptr = reinterpret_cast<const hash_T*>(dt->data());
            for (size_t i = 0; i < sz; ++i)
                hash_list.emplace_back(*(front_ptr + i));
        }
        else
            throw std::runtime_error("Isc file has no hash_data field.");
    }

    void plugins_load(const imsearch::IscFile* cfile, const imsearch::IscIndex* index)
    {
        for (auto& plugin : plugins)
        {
            plugin.get()->load(cfile, index);
        }
    }

public:

    using results_t = std::vector< std::unordered_map<std::string, std::string> >;

    int load_cfile(imsearch::BytesView data, const int index_type)
        // adminitrative function
        // loads, verifies and finds the correct indecies
        // then calls everything
        // @returns count of indecies loaded
    {
        /* From here to the next comment it takes about 530 microseconds for gzip, and 172us for LZ4*/
        std::vector<uint8_t> unpacked_data;
        if (*reinterpret_cast<const uint32_t*>(data.ptr) == 0x184D2204u)
            /* Magic number for the LZ4 frame format */
        {
            unpacked_data = Lz4File::decompress(data);
        }
        else if (*reinterpret_cast<const uint32_t*>(data.ptr + 4) == 0x46435349u)
            /* No compression, letters ISCF in flatbuffer file. */
        {
            unpacked_data = std::vector<uint8_t>{ data.cbegin(), data.cend() };
        }
        else
        {
            throw std::runtime_error("Unsupported file format.");
        }
        /* From here to before add_from_index takes less than 1 microsecond */
        auto tmp = flatbuffers::Verifier(
            reinterpret_cast<const uint8_t*>(&unpacked_data.front()), unpacked_data.size());
        const bool ok = imsearch::VerifyIscFileBuffer(tmp);
        if (!ok)
            throw std::invalid_argument("Corrupt data buffer. sz: " + std::to_string(unpacked_data.size()));

        const auto cfile = imsearch::GetIscFile(&unpacked_data.front());

        int count{ 0 };
        for (auto it = cfile->indecies()->cbegin(); it != cfile->indecies()->cend(); ++it)
        {
            if (it->type() == index_type)
            {
                ++count;
                /* this function takes about 215 microseconds without reserving space, and 96us with.*/
                load_iscindex(*it);

                /* this function takes about 30 microseconds */
                plugins_load(cfile, *it);
            }
        }
        return count;
    }


    void add_plugin(std::shared_ptr<FinderPlugin> plugin)
    {
        if (hash_list.size() != 0)
            throw std::runtime_error("Adding plugins while the finder has files loaded is bad. M'kay.");
        plugins.push_back(plugin);
    }


    void clear(bool keep_results)
    {
        hash_list.clear();
        if (!keep_results)
            result_vector.clear();

        for (auto& plugin : plugins)
        {
            plugin.get()->clear();
        }
    }

    uint64_t size() const noexcept { return hash_list.size(); }

    hash_T at(const int64_t i) const { return python_index(i, hash_list); }


    std::vector<FinderPlugin::dict_t> get_results(const hash_T query, const float similarity, const int max_results, bool clear)
    {
        // Expects( 0 < max_distance && 0 < max_results );

        std::vector<FinderPlugin::dict_t> list{};

        if (clear)
        {
            const auto native_sim = hash_T::from_similarity(similarity);
            result_vector.clear();
            result_vector.set_params(max_results, native_sim);
        }

        const auto search_results = search(query);

        for (const auto& result : search_results)
        {
            FinderPlugin::dict_t map{};
            map["distance"] = std::to_string(std::get<0>(result));

            for (auto& plugin : plugins)
            {
                map = plugin.get()->retrive_result(std::get<1>(result), map);
            }
            list.push_back(std::move(map));
        }

        return list;
    }


    std::vector<res_pair> search(const hash_T query)
    {
        constexpr size_t one = 1;

        const size_t list_size = hash_list.size() & ~one;

        for (size_t i = 0; i < list_size; i += 2)
        {
            const auto distance  = query.distance(hash_list[i]);
            const auto distance2 = query.distance(hash_list[i + 1]);
            const auto worst = result_vector.worst();

            if (distance < worst)
                result_vector.add(std::make_pair(distance, i), distance);

            else if (distance2 < worst)
                result_vector.add(std::make_pair(distance2, i), distance2);
        }

        if (hash_list.size() & 1u)
        {
            const auto distance = query.distance(hash_list.back());
            if (distance < result_vector.worst())
                result_vector.add(std::make_pair(distance, hash_list.size() - 1), distance);
        }

        return result_vector.get();
    }
};

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
