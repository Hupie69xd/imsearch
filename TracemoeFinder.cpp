// TracemoeFinder.cpp
// Copyright (C) 2022  Hupie (hupiew[at]gmail.com). License info at the end of this file.

#include <TracemoeFinder.hpp>

#include <algorithm>
#include <string>

#include <immintrin.h>

#define ANNOYLIB_MULTITHREADED_BUILD
#include <annoylib.h>
#include <kissrandom.h>


namespace imsearch {


/*#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <memoryapi.h>

std::string SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege);

#elif __GNUC__
#include <sys/mman.h>

#endif*/



/*#if __AVX512F__ && __AVX512BW__
constexpr int LANE_SIZE = 32;
#elif __AVX2__*/
constexpr int LANE_SIZE = 16; // Amount of 16b ints that fit into the SIMD register
//#endif

//constexpr int VEC_SIZE = TracemoeDesc::array_length; // Vector size
/*constexpr std::array<int8_t, TracemoeDesc::array_length> shiftMatrix = {
    1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    1, 0, 0, 0, 0, 0,
    2, 1, 1, 0, 0, 0 };*/



void TracemoeFinder::build_ann(int k_trees, int n_jobs)
{
    constexpr char fname[] = "TraceF_3.ann";

    auto ann = AnnoyTrace(TracemoeDesc::array_length);

    char* error;
    if (!ann.on_disk_build(fname, &error))
    {
        std::string err = error;
        free(error);
        throw std::runtime_error(err);
    }

    for (uint32_t i = 0; i < hash_list.size(); ++i)
    {
        std::array<float, 33> fl;
        for (int j = 0; j < 33; ++j)
            fl[j] = static_cast<float>(hash_list[i].coeffs[j]);

        const bool ok = ann.add_item(i, &fl[0], &error);
        if (!ok) [[unlikely]]
        {
            std::string err = error;
            free(error);
            throw std::runtime_error(err);
        }
    }

    hash_list.clear();
    hash_list.shrink_to_fit();

    if (!ann.build(k_trees, n_jobs, &error))
    {
        std::string err = error;
        free(error);
        throw std::runtime_error(err);
    }

    ann.unload();
}

bool TracemoeFinder::load_ann(const std::string& filename)
{
    bool ok = index.load(filename.c_str());
    hash_list.clear();
    hash_list.shrink_to_fit();
    return ok;
}

TracemoeFinder::results_t TracemoeFinder::search_ann(std::string query, int max_results)
{
    std::vector<int32_t> r;
    std::vector<float> d;
    std::array<float, 33> fl;
    for (int i = 0; i < 33; ++i)
        fl[i] = static_cast<float>(query[i]);
    index.get_nns_by_vector(&fl[0], max_results, -1, &r, &d);

    std::vector<FinderPlugin::dict_t> list{};
    for (int i = 0; i < r.size(); ++i)
    {
        FinderPlugin::dict_t map{};
        map["distance"] = std::to_string(d[i]);

        for (auto& plugin : plugins)
        {
            map = plugin.get()->retrive_result(r[i], map);
        }
        list.push_back(std::move(map));
    }

    return list;
}



/*pybind11::list TracemoeFinder::get_new(const TracemoeDesc query, const float similarity, const int max_results)
{
    // Expects( 0 < max_distance && 0 < max_results );

    pybind11::list list{};

    const auto search_results = new_search(query, similarity, max_results);

    for (const auto& result : search_results)
    {
        pybind11::dict dict{};
        dict["distance"] = std::get<0>(result);

        for (auto& py_obj : plugin_list)
        {
            auto plugin = py_obj.cast<FinderPlugin*>();
            dict = plugin->retrive_result(std::get<1>(result), dict);
        }
        list.append(std::move(dict));
    }

    return list;
}


std::vector<res_pair> TracemoeFinder::new_search(const TracemoeDesc query, const float similarity, const int max_results)
{
    if (avx_array == nullptr)
        transshuffel();

    const auto native_sim = TracemoeDesc::from_similarity(similarity);

    if (UINT16_MAX < native_sim)
        throw std::out_of_range("Threshold too high for the AVX function.");

    BestResultVector<res_pair> output(max_results, native_sim);

    __m256i thres = _mm256_set1_epi16( static_cast<uint16_t>(native_sim) );
    std::array<__m256i, 33> query_array;
    for (int i = 0; i < query_array.size(); ++i)
        query_array[i] = _mm256_set1_epi16(query.coeffs[i]);

    for (size_t i = 0; i < hash_list.size(); i += LANE_SIZE)
    {
        __m256i score = _mm256_setzero_si256();
        const size_t offset = i * VEC_SIZE;
        for (size_t j = 0; j < VEC_SIZE; ++j)
        {
            const __m256i query_byte = query_array[j];

            const __m128i a_16x8 = _mm_loadu_si128(reinterpret_cast<const __m128i*>(&avx_array[offset + j * LANE_SIZE]));
            const __m256i a_16x16 = _mm256_cvtepu8_epi16(a_16x8);
            // Start batch
            const __m256i d_16x16 = _mm256_sub_epi16(a_16x16, query_byte);

            const __m256i weighted = _mm256_slli_epi16(d_16x16, shiftMatrix[j]);

            const __m256i sq = _mm256_mullo_epi16(weighted, weighted);

            score = _mm256_adds_epu16(score, sq);
            // End batch
        }
        const __m256i d = _mm256_subs_epu16(score, thres);

        const __m256i x = _mm256_cmpeq_epi16(_mm256_setzero_si256(), d);

        uint32_t mask = _mm256_movemask_epi8(x);
        mask &= 0x5555555555555555ULL;

        if (mask == 0) [[likely]]
            continue;
        else           [[unlikely]]
            while (mask)
            {
                const uint64_t bit = _tzcnt_u64(mask);
                mask &= ~(1 << bit);

                const int index = (bit / 2) + i;

                const int distance = hash_list[index].distance({ query });
                if (distance < output.worst())
                {
                    output.add(std::make_pair(distance, index), distance);
                    thres = _mm256_set1_epi16(static_cast<uint16_t>(distance - 1));
                }
            }
    }
    return output.get();
}*/


void TracemoeFinder::reserve(size_t new_size)
{
    hash_list.reserve(new_size);
}


/*std::string SetPrivilege(
    HANDLE hToken,          // access token handle
    LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
    BOOL bEnablePrivilege   // to enable or disable privilege
)
{
    TOKEN_PRIVILEGES tp;
    LUID luid;

    if (!LookupPrivilegeValue(
        NULL,            // lookup privilege on local system
        lpszPrivilege,   // privilege to lookup
        &luid))        // receives LUID of privilege
    {
        std::string x(40, 0);
        sprintf(&x[0], "LookupPrivilegeValue error: %u\n", GetLastError());
        return &x.front();
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    // Enable the privilege or disable all privileges.

    if (!AdjustTokenPrivileges(
        hToken,
        FALSE,
        &tp,
        sizeof(TOKEN_PRIVILEGES),
        (PTOKEN_PRIVILEGES)NULL,
        (PDWORD)NULL))
    {
        std::string x(40, 0);
        sprintf(&x[0], "AdjustTokenPrivileges error: %u", GetLastError());
        return &x.front();
    }

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

    {
        return "The token does not have the specified privilege.";
    }

    return "TRUE";
}

void TracemoeFinder::transshuffel()
{
    const int n = hash_list.size();
    const int blocks_needed = n / LANE_SIZE + (n % LANE_SIZE != 0);
    const int bytes_needed = blocks_needed * LANE_SIZE * VEC_SIZE;

    auto phandle = GetCurrentProcess();
    HANDLE p;
    OpenProcessToken(phandle, TOKEN_ADJUST_PRIVILEGES, &p);
    const wchar_t* s = L"SeLockMemoryPrivilege";
    SetPrivilege(p, s, true);

    auto min_page_size = GetLargePageMinimum();
    const size_t remainder = bytes_needed % min_page_size;
    const size_t huge_pages_bytes = bytes_needed + ((min_page_size - remainder) * (remainder != 0));

    void* adrs = VirtualAlloc(NULL, huge_pages_bytes, MEM_COMMIT|MEM_RESERVE|MEM_LARGE_PAGES, PAGE_READWRITE);
    if (adrs != NULL)
    {
        avx_array = reinterpret_cast<uint8_t*>(adrs);
        pybind11::print("Large pages");
    }
    else
        avx_array = new uint8_t[bytes_needed];

    const int OFFSET_STEP = LANE_SIZE * VEC_SIZE;
    for (int i = 0; i < hash_list.size(); ++i)
    {
        const int offset = (i >> 4) * OFFSET_STEP + (i & 0xf);
        int step = 0;
        for (auto& item : hash_list[i].coeffs)
        {
            avx_array[offset + step] = item;
            step += LANE_SIZE;
        }
    }
}*/

} // namespace imsearch
/**
* Copyright (C) 2022  Hupie (hupiew[at]gmail.com)
*
*  This file is part of the ImSearch.
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  Or alternatively, under the terms of the GNU Lesser General Public
*  License as published by the Free Software Foundation, either version
*  3 of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU (Lesser) General Public License for more details.
*
*  You should have received a copy of the GNU (Lesser) General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
